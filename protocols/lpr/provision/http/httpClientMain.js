var axios = require('axios');
const log = require("../../../../global/console");
const settings = require('../../../../settings.json');
//update the main if change happend withe the alive of LPR camera
function sendCamStatusToMain(aliveUpdate) {
    return new Promise(async (resolve, reject) => {
        try {
            log.msg('sending updateing in alive to the main servie');
            let url = settings.mainService + "/cameras/update-alive-lpr-camera/";
            let response = await axios.post(url, aliveUpdate);

            if (response.status) {
                log.msg('Success on updateing alive in main service');
                resolve(response.data);
            }
        } catch (ex) {
            log.error('Failed to updateing alive in main service: ' + ex.message);
            reject(ex);
        }
    });
}

module.exports.sendCamStatusToMain = sendCamStatusToMain;