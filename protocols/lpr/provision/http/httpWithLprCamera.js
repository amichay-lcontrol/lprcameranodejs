const axios = require("axios");
const log = require("../../../../global/console");




// Make request to camera
async function makeRequest(lprCamera, requestData, actionMessage) {
    return new Promise(async (resolve, reject) => {

        let port = requestData["api"] == "/SetUnSubscribe" ? lprCamera.longPollingPort : lprCamera.Port;
        let url = `http://${lprCamera.Ip}:${port}${requestData["api"]}`;

        let basicAuth = `Basic ${Buffer.from(`${lprCamera.UserName}:${lprCamera.Password}`).toString('base64')}`;
        let requestHeaders = {
            Authorization: basicAuth
        };

        if (requestData["api"] == "/GetSnapshot") {
            requestHeaders["Accept"] = 'application/binary';
        }

        if (requestData["api"] == "/SetUnSubscribe") {
            requestHeaders["Accept"] = `*/*`;
            requestHeaders["Content-Type"] = `text/plain`;
            requestHeaders["Cache-Control"] = `no-cache`;
            requestHeaders["Host"] = `${lprCamera.Ip}:${port}`;
            requestHeaders["Accept-Encoding"] = `gzip, deflate, br`;
            requestHeaders["Content-Length"] = requestData["body"].length;
            requestHeaders["Connection"] = `keep-alive`;
            requestHeaders["User-Agent"] = `PostmanRuntime/7.22.0`;
            requestHeaders["Postman-Token"] = `5b9b1a61-94cc-451f-9a10-307eed9a7db9`;
        }

        log.msg(actionMessage + lprCamera.Name)
        try {
            if (requestData["body"]) {
                var response = await axios.post(url, requestData["body"], {
                    headers: requestHeaders
                });
                return resolve(response);
            } else {
                if (requestData["api"] == "/GetSnapshot") {
                    var response = await axios.get(url, {
                        responseType: 'arraybuffer',
                        headers: requestHeaders
                    });
                } else {
                    var response = await axios.get(url, {
                        headers: requestHeaders
                    });
                }
                return resolve(response);
            }
        } catch (ex) {
            log.error(`$makeRequest error - ${ex.message}`);
            return resolve({
                status: true,
                messsage: "not response from lpr camera"
            });
        }
    });
}

module.exports.makeRequest = makeRequest;