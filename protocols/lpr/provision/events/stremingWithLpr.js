const keepAlivingWithLpr = require("../alive/keepAlivingWithLpr");
const globalFunction = require("../../../../global/globalFunction");
const convertDateFormat = require('../../../../global/date');
const log = require("../../../../global/console");
const settings = require('./../../../../settings.json');
const http = require('../../../../http/http');
const plateFinder = require('../actions/findPlateByNumber');
const parkingManagement = require("../../general/parkingManagement");
const lprProvisionKeepAliving = require("../alive/keepAlivingWithLpr");

const net = require('net');
const convert = require('xml-js');
const axios = require('axios');
const fs = require("fs");

const convert_option = {
    compact: true,
    spaces: 4
}


let sentEvents = [];
let builtUnsubscribeConfig = false;
let nl = (process.platform === 'win32' ? '\r\n' : '\r\n');



//start lisining to events from the camera
async function listeningToEvent(lprCamera) {
    if (lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
        //clear the intervel of set renwe to the socket
        clearInterval(lprCamera["IntervelForSetRenew"]);

        try {
            log.msg("Getting LongPolling port: " + lprCamera.Name);
            var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + "/GetPortConfig";
            var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

            var response = await axios.get(url, {
                headers: {
                    Authorization: basicAuth
                }
            });

            if (response.status == 200) {
                let jsonResponse = convert.xml2json(response.data, convert_option);
                jsonResponse = JSON.parse(jsonResponse);

                lprCamera["longPollingPort"] = jsonResponse["config"]["port"]["longPollingPort"]["_text"];

            } else {
                log.error("Cannot get longPolling port from camera: " + lprCamera.Name);
                setTimeout(() => {
                    listeningToEvent(lprCamera);
                }, 3000);
            }
        } catch (ex) {
            log.error("error when try get long polling, try to start agaim im more 30 secounds, Error: " + ex.message);
            setTimeout(() => {
                listeningToEvent(lprCamera);
            }, 30000);
        }


        //afetr we get the long poling port start listening to event from the socket
        if (lprCamera["longPollingPort"] && lprCamera["supportAPILongPolling"]) {

            let a = fs.readFileSync('provisionLprCameraSubscribeConfig.xml');
            let subscribeConfig = fs.readFileSync('provisionLprCameraSubscribeConfig.xml', {
                encoding: 'utf-8'
            });
            var nl = (process.platform === 'win32' ? '\r\n' : '\r\n');
            let header = `POST /SetSubscribe HTTP/1.1${nl}` +
                `Host: ${lprCamera.Ip}:${lprCamera.longPollingPort}${nl}` +
                `Authorization: ${basicAuth}${nl}` +
                `Connection: Keep-Alive${nl}` +
                `Content-Type: application/x-www-form-urlencoded${nl}` +
                `Content-Length: ${subscribeConfig.length}${nl}${nl}`;
            subscribeConfig = header + subscribeConfig;

            let t = 3;

            try {
                lprCamera["socket"] = net.Socket();

                lprCamera["socket"].connect(lprCamera.longPollingPort, lprCamera.Ip, async function () {
                    log.msg(`Socket connected: ${lprCamera.Name}`);
                    lprCamera["socketConnected"] = true;
                    let response = lprCamera["socket"].write(subscribeConfig);

                    if (response) {
                        log.info(lprCamera.Name + ": start listing events from LPR camera");

                        //set renew every 1 minutes to check that the socket still open 
                        lprCamera["IntervelForSetRenew"] = setInterval(async () => {
                            if (lprCamera["socket"])
                                setRenewToSocket(lprCamera);
                            /*  else {
                                 lprCamera["IntervelForSetRenew"] = null;
                                 if (lprCamera["IsAlive"]) {
                                     //try connect again to the camera
                                     await listeningToEvent(lprCamera);
                                 }
                             } */
                        }, 30000);
                    }

                    //keepAlivingWithLpr.activeTimeout(lprCamera);
                });

                lprCamera["socket"].on('data', async function (data) {
                    try {
                        //log.info("###############  " + lprCamera.Name);
                        lprCamera['StremingData'] += data;

                        //clear the timeOut, and active the lpr to alive
                        /* keepAlivingWithLpr.clearAliveLprTimeout(lprCamera); */

                        // active timeOut that will check in 1 secounds if we alive or not
                        // if this timeOut will be alive is mean that we not get other sign from the LPR camera
                        // and we stop the alive of the LPR camera
                        /*  keepAlivingWithLpr.activeTimeout(lprCamera); */

                        // check data from camera till get connection data to build unsubs
                        if (!builtUnsubscribeConfig && lprCamera['StremingData'].includes("<serverAddress") && lprCamera['StremingData'].includes("</serverAddress")) {
                            let indexOfOpenTagStreamingData = lprCamera['StremingData'].indexOf('<serverAddress');
                            let indexOfCloseTagStreamingData = lprCamera['StremingData'].indexOf('</serverAddress');
                            lprCamera["unsubscribeData"] = lprCamera['StremingData'].substring(indexOfOpenTagStreamingData + 30, indexOfCloseTagStreamingData);
                            builtUnsubscribeConfig = true;
                        }

                        let indexOfClosingTag = lprCamera['StremingData'].indexOf('</config>');

                        if (indexOfClosingTag != -1) {
                            indexOfClosingTag += '</config>'.length
                            lprCamera["dataToCheck"] = lprCamera['StremingData'].substring(0, indexOfClosingTag);
                            lprCamera['StremingData'] = lprCamera['StremingData'].substring(indexOfClosingTag);

                            //checkStreamData(lprCamera);

                            checkMessagesFromSocket(lprCamera);
                        }

                    } catch (ex) {
                        log.error(`Error: ${ex}`);
                    }
                });

                //when end the socket
                lprCamera["socket"].on('end', async function (error) {
                    log.error(`End socket with lpr-camera: ${lprCamera.Name}, Error: ${error}`);
                    lprCamera["socket"] = null;

                    try {
                        //     SetUnSubscribe     //  
                        //lprCamera["socket"] = setUnSubScribeToSocket(lprCamera);

                        //     SetRenew     //
                        //lprCamera["socket"] = setRenewToSocket(lprCamera);
                    } catch (ex) {
                        log.error("Error when try set renew to lpr camera after End socket, Error:" + ex.message)
                    }

                    //try connect again
                    /*   if (lprCamera["IsAlive"])
                          //try connect again to the camera
                          await listeningToEvent(lprCamera); */
                });

                //when close the socket
                lprCamera["socket"].on('close', async function (error) {
                    lprProvisionKeepAliving.clearAliveLprTimeout(lprCamera);
                    log.error(`Closed socket with lpr-camera: ${lprCamera.Name}, Error:  ${error}`);
                    lprCamera["socket"] = null;

                    //clear timeout for updateing the connection with the socket
                    clearTimeout(lprCamera["timeoutForNewConnectToSocket"]);

                    //lprCamera["longPollingPort"] = null;
                    //lprCamera["socketConnected"] = false;


                    try {
                        //     SetUnSubscribe     // 
                        //lprCamera["socket"] = setUnSubScribeToSocket(lprCamera);

                        //     SetRenew     //
                        //lprCamera["socket"] =   setRenewToSocket(lprCamera);
                    } catch (ex) {
                        log.error("Error when try set renew to lpr camera after Close socket, Error:" + ex.message)
                    }


                    //afetr the socket failes
                    lprCamera["timeoutForNewConnectToSocket"] = setTimeout(async () => {
                        //check if the lpr camera is alive but the socket is null 
                        if (lprCamera["IsAlive"] && !lprCamera["socket"]) {
                            clearInterval(lprCamera["IntervelForSetRenew"]);
                            lprCamera["IntervelForSetRenew"] = null;
                            //try connect again to the camera
                            await listeningToEvent(lprCamera);
                        }
                    }, 60000);
                });

                //when error in socket
                lprCamera["socket"].on('error', async function (error) {
                    log.error(`Error socket with lpr-camera: ${lprCamera.Name}, Error:  ${error.message}`);

                    //try connect again to the camera
                    /* if (lprCamera["IsAlive"])
                        await listeningToEvent(lprCamera); */
                });

            } catch (ex) {
                log.error(`listeningToEvent error ${ex}`);
            }
        } else {
            log.error("This camera not have polling port camera: " + lprCamera.Name);
        }
    }
}


//check if in the stream there is a new event
async function checkStreamData(lprCamera) {
    try {
        let start = lprCamera['dataToCheck'].search("<config");
        let end = lprCamera['dataToCheck'].search("</config>");

        /*  let start = message.search("<config");
         let end = message.search("</config>"); */

        if (start != -1 && end != -1) {
            let xmlData = lprCamera['dataToCheck'].slice(start, end + 10);
            //chack every message that start with  `<?xml`
            //if no cut the message from the  `<?xml` until to  `</config>`
            //check messages from the socket
            /*  */
            //log.specialMsg('XmlData length: ' + xmlData.length);
            //log.specialMsg('dataToCheck length: ' + lprCamera['dataToCheck'].length);
            //log.specialMsg('StremingData length: ' + lprCamera['StremingData'].length);

            let jsonResponse;
            try {
                jsonResponse = convert.xml2json(xmlData, convert_option);
            } catch (err) {
                log.error("Error when try converting xml to json from Lpr camera:" + lprCamera.Name + ", Error: " + err.message);
            }
            jsonResponse = await JSON.parse(jsonResponse);

            if (jsonResponse && jsonResponse["config"]["plateCount"] && jsonResponse["config"]["plateCount"]["_text"] == "1") {
                let eventName = jsonResponse["config"]["smartType"]["_text"];
                //update that we get new message
                newEvent(lprCamera, eventName, end);
            } else {
                cleanStreamData(lprCamera, end);
            }
        }
    } catch (ex) {
        log.error("error when check strem data, Error: " + ex.message);
    }
}

//cheke if exist `<?xml`
//and check if more then one message
function checkMessagesFromSocket(lprCamera) {
    try {
        let messages = lprCamera['dataToCheck'];

        let start = messages.search("<config");
        let end = messages.search("</config>");
        let xmlData = messages.slice(start, end + 10);

        //check if exist `<?xml` so cut until this `<?xml`, and call the function again
        if (xmlData.indexOf('<?xml') > 0) {
            //cut the string that before `<?xml`
            lprCamera['dataToCheck'] = lprCamera['dataToCheck'].substring(0, xmlData.indexOf('<?xml') + 5);
            //cal the function again;
            checkMessagesFromSocket(lprCamera);
        } else
            checkStreamData(lprCamera);
    } catch (ex) {
        log.error("error when try check the xml data, Error: " + ex.message);
    }
}

//clean the stream data
function cleanStreamData(lprCamera, end) {
    lprCamera['StremingData'] = lprCamera['StremingData'].substring(end + 10, lprCamera['StremingData'].lenght);
}

//new event except
async function newEvent(lprCamera, eventName, end) {
    //check the event type
    switch (eventName) {
        case "VEHICE": {
            //log.info('Get new event of:' + eventName + ', from LPR camera: ' + lprCamera.Name);
            //get from the lpr camera the event of new car detact
            var events = await getTheCarDetection(lprCamera);
            //send the events to the main service
            for (let event of events) {
                //check if this event not send yet
                if (true) {
                    //(checkInSentEvents(event)) {
                    sentEvents.push(event);

                    log.specialMsg(" ***** New event *****")
                    log.specialMsg("Car number - " + event.carNumber);
                    log.specialMsg("Date - " + event.eventTime);
                    log.specialMsg("Arrived in time - " + event.eventAcceptedInTime);
                    if (event.eventType == 1)
                        log.specialMsg("Allowed car - this car in white list");
                    else if (event.eventType == 0)
                        log.specialMsg("Not allowed car - this car in black list");
                    else if (event.eventType == 2)
                        log.specialMsg("Not allowed car - this Unknown car");
                    log.specialMsg(` ***** ${lprCamera.Name} ***** `);

                    log.info('Succesfully update the latest event, from LPR camera: ' + lprCamera.Name);
                    lastEventTime = event["eventTime"];


                    try {
                        let response = await http.updateNewLprCameraEvent(event);
                        if (response) {
                            log.msg("data was send to main service");
                        } else {
                            log.msg("Not internet connection , save data in file ")
                        }
                    } catch (ex) {
                        log.error("Error when try sending event to main service, Error: " + ex.message);
                    }


                    /* ****************************************** */
                    //     this system for manage the parking     //
                    /* ****************************************** */
                    //check if the provision camera is the meneger of the parking
                    //PROVISION
                    if (settings.camManager == 2) {
                        //if new allowed car entered, save this car in the list of existing cars in the parking
                        if (event.eventType == 1) {
                            try {
                                await parkingManagement.updateExistingCars(event, lprCamera);

                                //update parking state in main
                                let parkingData = {};
                                //get the exist car list from the file
                                let existingCarData = fs.readFileSync("company_parking_count.txt");
                                existingCarData = JSON.parse(existingCarData);

                                //check if found company for this car number
                                if (event.companyId && existingCarData[event.companyId]) {
                                    if (existingCarData[event.companyId].existingCars)
                                        parkingData[event.companyId] = {
                                            cars: existingCarData[event.companyId].existingCars,
                                            occupiedParkingSpace: existingCarData[event.companyId].existingCars.length
                                        }

                                    //update main on change in occupied parking space
                                    try {
                                        let response = await http.updateOccupiedParkingSpace(parkingData);
                                    } catch (ex) {
                                        log.error("error when try update main service on update in occupied parking space, Error: " + ex.message)
                                    }
                                } else {
                                    log.error("Error this is allowed car number but not found in list of users, carNumber: " + event.carNumber);
                                }
                            } catch (ex) {
                                log.error("Error when try update in existing car, Error: " + ex.message);
                            }
                        }
                    }
                }
            }
            cleanStreamData(lprCamera, end);


            //if we found a complate message in the stream 
            //and we analyzed her information, so clean her

            break;
        }
        case "MOTION": {
            //log.info('Get new event of:' + eventName + ', from LPR camera: ' + lprCamera.Name);


            break;
        }
        case "videoloss": {

            //clear the timeOut, and active the lpr to alive
            keepAlivingWithLpr.clearAliveLprTimeout(lprCamera);

            // active timeOut that will check in 1 secounds if we alive or not
            // if this timeOut will be alive is mean that we not get other sign from the LPR camera
            // and we stop the alive of the LPR camera
            keepAlivingWithLpr.activeTimeout(lprCamera);

            break;
        }
        default: {
            log.info('******************************');
            log.info('Get new event of:' + eventName + ', from LPR camera: ' + lprCamera.Name);
            log.info('******************************');
            break;
        }
    }
}

//
async function getTheCarDetection(lprCamera) {
    try {
        var events = [];
        var eventXml = lprCamera.dataToCheck;
        var evXmls = eventXml.split(nl + nl);
        var promises = [];

        for (let i = 0; i < evXmls.length; i++) {
            xmlEvent = evXmls[i];

            try {

                let jEvent = globalFunction.convertXmlToJson(xmlEvent);

                //console.log(xmlEvent);
                let items = jEvent["config"]["listInfo"]["item"];
                let event = {
                    serialNumber: settings.serialNumber,
                };

                event["lprCameraId"] = lprCamera.CameraId;
                event["carNumber"] = items[1]["plateNumber"]["_cdata"];
                event["eventImage"] = `data:image/jpeg;base64,${items[1]["targetImageData"]["targetBase64Data"]["_cdata"]}`;
                event["snapshot"] = `data:image/jpeg;base64,${items[0]["targetImageData"]["targetBase64Data"]["_cdata"]}`;

                var milliseconds = jEvent["config"]["currentTime"]["_text"];
                let newlength = (milliseconds.length) - 3;
                milliseconds = milliseconds.substring(0, newlength);
                event["eventTime"] = new Date(+milliseconds);
                //let timeZoneOffset = new Date().getTimezoneOffset();
                let timeZoneOffset = -60;
                event["eventTime"] = convertDateFormat(new Date(event["eventTime"].getTime() + timeZoneOffset * 60000), "Date", "yyyy-MM-dd HH:mm:ss");
                let plate = await plateFinder.findPlateByNumber(lprCamera, event["carNumber"]);
                if (!plate) {
                    event["eventType"] = 2;
                } else {
                    event["eventType"] = plate.allowlistValue == "blackList" ? 0 : plate.allowlistValue == "whiteList" ? 1 : 2;
                }

                event.eventAcceptedInTime = convertDateFormat(new Date(), "Date", "yyyy-MM-dd HH:mm:ss");
                events.push(event);

            } catch (ex) {

            }
        }
    } catch (error) {
        log.error('Error in LPR camera :' + lprCamera.Name + ", cannot get the last events," + error.message);
        return [];
    }

    return events;

}



//setUnSubScribe
async function setUnSubScribeToSocket(lprCamera) {
    try {
        log.msg("Try set unSubscribe to lpr camera socket: " + lprCamera.Name);

        if (!lprCamera["unsubscribeData"]) {
            lprCamera["unsubscribeData"] = `<![CDATA[http://${lprCamera.Ip}:${lprCamera.longPollingPort}/TVT/event/subsription_0]]>`
        }

        let nl = (process.platform === 'win32' ? '\r\n' : '\r\n');
        let basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');


        //     SetUnSubscribe     //  
        let xml = `<?xml version="1.0" encoding="UTF-8"?>${nl}` +
            `<config version="1.7" xmlns="http://www.ipc.com/ver10">${nl}` +
            `<types>${nl}` +
            `<openAlramObj>${nl}` +
            `<enum>MOTION</enum>${nl}` +
            `<enum>SENSOR</enum>${nl}` +
            `<enum>PEA</enum>${nl}` +
            `<enum>AVD</enum>${nl}` +
            `<enum>OSC</enum>${nl}` +
            `<enum>CPC</enum>${nl}` +
            `<enum>CDD</enum>${nl}` +
            `<enum>IPD</enum>${nl}` +
            `<enum>VFD</enum>${nl}` +
            `<enum>VFD_MATCH</enum>${nl}` +
            `<enum>VEHICE</enum>${nl}` +
            `<enum>AOIENTRY</enum>${nl}` +
            `<enum>AOILEAVE</enum>${nl}` +
            `<enum>PASSLINECOUNT</enum>${nl}` +
            `<enum>TRAFFIC</enum>${nl}` +
            `</openAlramObj>${nl}` +
            `<subscribeRelation>${nl}` +
            `<enum>ALARM</enum>${nl}` +
            `<enum>FEATURE_RESULT</enum>${nl}` +
            `<enum>ALARM_FEATURE</enum>${nl}` +
            `</subscribeRelation>${nl}` +
            `<subscribeTypes>${nl}` +
            `<enum>BASE_SUBSCRIBE</enum>${nl}` +
            `<enum>REALTIME_SUBSCRIBE</enum>${nl}` +
            `<enum>STREAM_SUBSCRIBE</enum>${nl}` +
            `</subscribeTypes>${nl}` +
            `</types>${nl}` +
            `<serverAddress type="string">${lprCamera["unsubscribeData"]}</serverAddress>${nl}` +
            `<unsubscribeList type="list" count="1">${nl}` +
            `<item>${nl}` +
            `<smartType type="openAlramObj">VEHICE</smartType>${nl}` +
            `<subscribeRelation type="subscribeRelation">ALARM_FEATURE</subscribeRelation>${nl}` +
            `</item>${nl}` +
            `</unsubscribeList>${nl}` +
            `</config>${nl}`;

        let header = `POST /SetUnSubscribe HTTP/1.1${nl}` +
            `Host: ${lprCamera.Ip}:${lprCamera.longPollingPort}${nl}` +
            `Authorization: ${basicAuth}${nl}` +
            `Content-Type: application/xml;charset=utf-8${nl}` +
            `Content-Length: ${xml.length}${nl}${nl}`;
        let SetUnSubscribe = header + xml;


        //send to socket
        let response = lprCamera["socket"].write(SetUnSubscribe);
        if (response)
            log.info('Succesfully set unSubscribe, to LPR camera: ' + lprCamera.Name);
        return response;
    } catch (ex) {
        log.error("Error when try set unsubscribe to the socket in lpr camera: " + lprCamera.Name + ", Error:" + ex.message);
    }
}


//setRenew
async function setRenewToSocket(lprCamera) {
    try {
        log.msg("Try set renew to lpr camera socket:" + lprCamera.Name);

        if (!lprCamera["unsubscribeData"]) {
            lprCamera["unsubscribeData"] = `<![CDATA[http://${lprCamera.Ip}:${lprCamera.longPollingPort}/TVT/event/subsription_0]]>`
        }

        let nl = (process.platform === 'win32' ? '\r\n' : '\r\n');
        let basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');


        //     SetRenew     //  
        let xml = `<?xml version="1.0" encoding="UTF-8"?>${nl}` +
            `<config version="1.7" xmlns="http://www.ipc.com/ver10">${nl}` +
            `<serverAddress type="string">${lprCamera["unsubscribeData"]}</serverAddress>${nl}` +
            `<renewTime type="uint32">55</renewTime>${nl}` +
            `</config>${nl}`;

        let header = `POST /SetRenew HTTP/1.1${nl}` +
            `Host: ${lprCamera.Ip}:${lprCamera.longPollingPort}${nl}` +
            `Authorization: ${basicAuth}${nl}` +
            `Content-Type: application/xml;charset=utf-8${nl}` +
            `Content-Length: ${xml.length}${nl}${nl}`;
        let SetRenew = header + xml;


        //send to socket
        let response = lprCamera["socket"].write(SetRenew);
        if (response) {
            //clear timeout for updateing the connection with the socket
            clearTimeout(lprCamera["timeoutForNewConnectToSocket"]);

            log.info('Succesfully set renwe, to LPR camera: ' + lprCamera.Name);
        }
        return response;
    } catch (ex) {
        log.error("Error when try set renew to the socket in lpr camera: " + lprCamera.Name + ", Error:" + ex.message);
    }
}

module.exports.listeningToEvent = listeningToEvent;
module.exports.setUnSubScribeToSocket = setUnSubScribeToSocket;
module.exports.setRenewToSocket = setRenewToSocket;