const connectionWithLpr = require("./connectionWithLpr");
const axios = require('axios');
const log = require("../../../../global/console");
const convertDateFormat = require('../../../../global/date');
const mainHttpConnection = require('../http/httpClientMain');
const stremingWithLpr = require("../events/stremingWithLpr");



/*******************************/
/***** timeout - for alive *****/
/*******************************/
//this function clear the lprTimeout and keep alive for the LPR camera
function clearAliveLprTimeout(lprCamera) {
    if (lprCamera["IsAliveNeedUpdate"]) {
        lprCamera["IsAlive"] = true;
        lprCamera["IsAliveNeedUpdate"] = false;

        //send message to the main service that the connection start 
        updateingLprCamera(lprCamera);
    }

    //clean time out
    clearTimeout(lprCamera["ConnectionTimeout"]);
    lprCamera["ConnectionTimeout"] = null;
}


//we add for this lprCamera Timeout
async function activeTimeout(lprCamera) {
    //start Timeout
    lprCamera["ConnectionTimeout"] = setTimeout(async () => {

        //check if camera is alive
        try {
            log.msg("try get alive from camera: " + lprCamera.Name);
            var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + "/GetDeviceInfo";
            var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');
            var response = await axios.get(url, {
                headers: {
                    Authorization: basicAuth
                }
            });
            if (response && response.status == 200)
                lprCamera["IsAlive"] = true;
            else
                lprCamera["IsAlive"] = false;
        } catch (ex) {
            log.error("error when try get alive from lpr camera: " + lprCamera.Name + ", Error:" + ex.message);
        }



        if (lprCamera["IsAlive"]) {
            log.info("we not get response from lpr camera, so we chack alive, lpr camera : " + lprCamera.Name + " is alive.");

            //check if memory stream is open
            if (!lprCamera["socket"]) {
                //close the socket connection

                log.info("The strem with this lpr camera was close, we open new one, with lpr camera: " + lprCamera.Name);

                //start listinner to event from LPR cameras
                await stremingWithLpr.listeningToEvent(lprCamera);
            }
            // if say the socket is open, we will check after X time if still not get data in the socket we close it and open again
            else {
                lprCamera["ConnectionTimeout"] = setTimeout(() => {
                    activeTimeout(lprCamera);

                    log.info("we close socket and open again,  lpr camera : " + lprCamera.Name + " is alive.");
                    //set unsubscribe after socket closed
                    stremingWithLpr.setUnSubScribeToSocket(lprCamera);
                    //close the socket connection
                    lprCamera["socket"].destroy();

                }, 5000)
            }
        } else {
            try {
                lprCamera["IsAliveNeedUpdate"] = true;
                lprCamera['StremingData'] = "";

                //if tthe socket is open we will close it
                if (lprCamera["socket"]) {
                    log.info("we close socket and open again,  lpr camera : " + lprCamera.Name + " is alive.");

                    //set unsubscribe after socket closed
                    stremingWithLpr.setUnSubScribeToSocket(lprCamera);
                    //close the socket connection
                    lprCamera["socket"].destroy();
                }

                //send message to the main service thet the connection stop
                updateingLprCamera(lprCamera);

                //try connect again to the lpr camera
                connectionWithLpr.getCamerasAlive(lprCamera);
            } catch (ex) {
                log.error("error when try connect again to lpr camera: " + lprCamera.Name + ", Error:" + ex.message)
            }
        }
    }, 10000);
}




//updateing lpr camera Alive Status in main service
async function updateingLprCamera(lprCamera) {
    if (lprCamera.IsAlive)
        log.info(lprCamera.Name + ": Connect again ");
    else
        log.error(lprCamera.Name + ": Disconnect ");

    let dataToSend = {
        LprCameraId: lprCamera.CameraId,
        IsAlive: lprCamera.IsAlive,
        EventTime: convertDateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss')
    }
    let response = await mainHttpConnection.sendCamStatusToMain(dataToSend);
}

module.exports.clearAliveLprTimeout = clearAliveLprTimeout;
module.exports.activeTimeout = activeTimeout;