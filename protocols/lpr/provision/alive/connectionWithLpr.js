const log = require("../../../../global/console");
const globalFunction = require("../../../../global/globalFunction");
const setDate = require("../actions/dateTimeInLpr");
const stremingWithLpr = require("../events/stremingWithLpr")
const axios = require('axios');

//get alive for camera
async function getCamerasAlive(lprCamera) {
    return new Promise(async function (resolve, reject) {
        try {
            log.msg("try get alive from camera: " + lprCamera.Name);
            var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + "/GetDeviceInfo";
            var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');
            var response = await axios.get(url, {
                headers: {
                    Authorization: basicAuth
                }
            });

            if (response && response.status == 200) {
                log.info(lprCamera.Name + ": this camera is alive");
                lprCamera["IsAlive"] = true;
                lprCamera["IsAliveNeedUpdate"] = true;

                let jsonResponse = globalFunction.convertXmlToJson(response.data);

                //in case if this is lpr camera
                let support = jsonResponse["config"]["deviceInfo"]["supportVehice"] && (jsonResponse["config"]["deviceInfo"]["supportVehice"]["_text"] == 'true');
                if (support) {
                    lprCamera["supportAPILongPolling"] = jsonResponse["config"]["deviceInfo"]["supportAPILongPolling"]["_text"] == 'true';
                    log.info(lprCamera.Name + ": this camera is LPR,")
                    lprCamera['IsLpr'] = true;

                    //update date time in lpr camera
                    //_ = await setDate.updateDateTimeInCamera(lprCamera);


                    //start listinner to event from LPR cameras
                    _ = await stremingWithLpr.listeningToEvent(lprCamera);
                }

                resolve(true);
            } else {
                log.error("Camera : " + lprCamera.Name + ", not connected")
                lprCamera["IsAlive"] = false;

                //try connect again to the lpr camera
                resolve(getCamerasAlive(lprCamera));
            }
        } catch (ex) {
            log.error("Error in camera : " + lprCamera.Name + "; " + ex.message);
            lprCamera["IsAlive"] = false;

            //try connect again to the lpr camera
            getCamerasAlive(lprCamera);

            reject(false);
        }
    });
}

module.exports.getCamerasAlive = getCamerasAlive;