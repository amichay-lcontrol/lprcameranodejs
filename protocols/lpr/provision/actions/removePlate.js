const httpLpr = require('../http/httpWithLprCamera');
const plateFinder = require('./findPlateByNumber');


//to remove by car number
async function removePlate(lprCamera, plateNumber) {
    let plate = await plateFinder.findPlateByNumber(lprCamera, plateNumber);
    if (plate) {
        let plateId = plate.id;
        let requestData = {
            body: `<?xml version=\"1.0\" encoding=\"utf-8\"?>` +
                `<config>` +
                `<vehiclePlates>` +
                `<keyList type=\"list\" count=\"1\">` +
                `<item>` +
                `<keyId type =\"unit32\">${plateId}</keyId>` +
                `</item>` +
                `</keyList>` +
                `<listType>` +
                `</listType>` +
                `<carPlateNum><![CDATA[]]></carPlateNum>` +
                `</vehiclePlates>` +
                `</config>`,
            api: `/DeleteVehiclePlate`
        }
        let response = await httpLpr.makeRequest(lprCamera, requestData, ` remove plate: ${plateNumber} on camera: `);
        if (response.status == 200) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}


//to remove some cars number
async function removeFromPlatesFromList(lprCamera, carList) {
    return new Promise(async (resolve, reject) => {
        var results = [];
        for (var i = 0; i < carList.length; i++) {
            var response = await removePlate(lprCamera, carList[i])
            var removeResponseObject = {
                car: carList[i],
                status: response
            }
            results.push(removeResponseObject);
        }
        resolve(results);
    })
}

module.exports.removePlate = removePlate;
module.exports.removeFromPlatesFromList = removeFromPlatesFromList;