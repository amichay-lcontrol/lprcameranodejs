const lprParkingManagement = require("../../general/parkingManagement");
const programControl = require("../../../../program-control");
const httpLpr = require('../http/httpWithLprCamera');
const findPlateByNumber = require("./findPlateByNumber");
const removePlate = require("./removePlate");
const log = require('../../../../global/console');

// check before that add car number to white list
//****step 1****
async function addPlateToWhiteList(lprCamera, plates) {
    return new Promise(async (resolve, reject) => {
        var results = [];

        //update users and company from DB
        //await programControl.getAndUpdateDataFromDB();

        //check if this car already exist in the lpr camera
        //get the car list in this lpr camera
        let carListInLprCamera = await findPlateByNumber.findAllPlateNumber(lprCamera);

        for (let plate of plates) {
            let addResponseObject;

            //if this car exit in the list, so we update it to white list
            if (carListInLprCamera &&
                Number(carListInLprCamera["config"]["vehiclePlates"]["_attributes"]["count"]) == 1 &&
                carListInLprCamera["config"]["vehiclePlates"]["item"]["carPlateNumber"] == plate) {

                //check if this car number is white list
                if (carListInLprCamera.filter((car) => {
                        (car.CarNumber == plate && car["plateItemType"] != 'whiteList') ? true: false;
                    })) {
                    //remove it
                    await removePlate.removePlate(lprCamera, plate);
                    //add this car number to white list
                    addResponseObject = await addCarNumberAsWhiteList(lprCamera, plate);
                }
            } else if (carListInLprCamera &&
                Number(carListInLprCamera["config"]["vehiclePlates"]["_attributes"]["count"]) > 1 &&
                carListInLprCamera["config"]["vehiclePlates"]["item"].filter((car) => car["carPlateNumber"] == plate).length > 0) {

                //check if this car number is white list
                if (carListInLprCamera.filter((car) => {
                        (car.CarNumber == plate && car["plateItemType"] != 'whiteList') ? true: false;
                    })) {
                    //remove it
                    await removePlate.removePlate(lprCamera, plate);
                    //add this car number to white list
                    addResponseObject = await addCarNumberAsWhiteList(lprCamera, plate);
                }
            }
            //if this car not exist in the lpr, add him
            else {
                addResponseObject = await addCarNumberAsWhiteList(lprCamera, plate);
            }

            results.push(addResponseObject);
        }
        resolve(results);
    }).catch(function (error) {
        log.error("error happend when try to checking car number, Error:" + error.message);
        addResponseObject["status"] = false;
        addResponseObject["message"] = "Error happend when try to add car number to the lpr camrea";

        reject(results);
    });
}


//add car number as white list to the lpr camera
//****step 2****
async function addCarNumberAsWhiteList(lprCamera, plate) {
    return new Promise(async (resolve, reject) => {
        var results = [];

        //check if this car belong to company that the parking space of her is full 
        let fullParkingOrNotPrivateParkingSpace = await lprParkingManagement.checkIfCompanyFullParkingSpace(plate);

        //if have parking spaces for him
        if (!fullParkingOrNotPrivateParkingSpace) {
            let requestData = {
                body: `<?xml version="1.0" encoding="utf-8"?>` +
                    `<config>\n<vehiclePlates type="list" count="1">` +
                    `<item>` +
                    `<carPlateNumber type ="string"><![CDATA[${plate}]]></carPlateNumber>` +
                    `<beginTime type ="string"><![CDATA[2020/01/01 00:00:00}]]></beginTime>` +
                    `<endTime type ="string"><![CDATA[2030/01/01 00:00:00]]></endTime>` +
                    `<carPlateColor type ="string"><![CDATA[Green]]></carPlateColor>` +
                    `<carPlateType type ="string"><![CDATA[1566]]></carPlateType>` +
                    `<carType type ="unit32"><![CDATA[1566]]></carType>` +
                    `<carOwner type ="string"><![CDATA[CardHolder]]></carOwner>` +
                    `<carColor type ="string"><![CDATA[white]]></carColor>` +
                    `<plateItemType type ="string">whiteList</plateItemType>` + //whiteList 
                    `</item>` +
                    `</vehiclePlates>` +
                    `</config>`,
                api: `/AddVehiclePlate`
            }

            let response = await httpLpr.makeRequest(lprCamera, requestData, `Add car number -${plate}- to white plates list, lpr camera: `);
            if (response) {
                /*  lprCamera["insercCarsNum"]++;
                            log.msg(lprCamera["insercCarsNum"] + ",  cars addedd to the lpr camera"); */
                var addResponseObject = {
                    car: plate
                }
                addResponseObject["status"] = (response.status == 200) ? true : false;
                addResponseObject["message"] = "Add to lpr camera succesfully";
            } else {
                var addResponseObject = {
                    car: plate
                }
                addResponseObject["status"] = false;
                addResponseObject["message"] = "lpr camera not response to add car number";
                log.error("lpr camera not response when try to add car number, lpr amera: " + lprCamera.Name);
            }

        } else {
            log.msg("the parking spaces for this Compaany is full, so we erite this car number in the list until will have place in the parking");
            var addResponseObject = {
                car: plate
            }
            addResponseObject["status"] = true;
            addResponseObject["message"] = "Addes to the list , the parking spaces for this Compaany is full";
        }

        //results.push(addResponseObject);
        resolve(addResponseObject);
    }).catch(function (error) {
        log.error("error happend when try to add car number to the lpr camrea, Error:" + error.message);
        addResponseObject["status"] = false;
        addResponseObject["message"] = "Error happend when try to add car number to the lpr camrea";

        reject(results);
    });
}

module.exports.addPlateToWhiteList = addPlateToWhiteList;