const httpLpr = require('../http/httpWithLprCamera');

// Add vehicle plate
async function addPlateToBlackList(lprCamera, plates) {
    var results = [];
    return new Promise(async(resolve,reject)=>{
        for (let plate of plates) {
            let requestData = {
                body: `<?xml version="1.0" encoding="utf-8"?>` +
                    `<config>\n<vehiclePlates type="list" count="1">` +
                    `<item>` +
                    `<carPlateNumber type ="string"><![CDATA[${plate}]]></carPlateNumber>` +
                    `<beginTime type ="string"><![CDATA[2020/01/01 00:00:00}]]></beginTime>` +
                    `<endTime type ="string"><![CDATA[2030/01/01 00:00:00]]></endTime>` +
                    `<carPlateColor type ="string"><![CDATA[Green]]></carPlateColor>` +
                    `<carPlateType type ="string"><![CDATA[1566]]></carPlateType>` +
                    `<carType type ="unit32"><![CDATA[1566]]></carType>` +
                    `<carOwner type ="string"><![CDATA[CardHolder]]></carOwner>` +
                    `<carColor type ="string"><![CDATA[black]]></carColor>` +
                    `<plateItemType type ="string">blackList</plateItemType>` +  
                    `</item>` +
                    `</vehiclePlates>` +
                    `</config>`,
                api: `/AddVehiclePlate`
            }
            let response = await httpLpr.makeRequest(lprCamera, requestData , `${plate} added to black plates list, camera: `);
            var remResponseObject = {
                car: plate
            }
            remResponseObject["status"] = (response.status == 200) ? true : false;
            results.push(remResponseObject);
        }
         resolve(results);
    });
}

module.exports.addPlateToBlackList = addPlateToBlackList;