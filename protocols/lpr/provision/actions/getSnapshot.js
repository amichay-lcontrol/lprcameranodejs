const httpLpr = require('../http/httpWithLprCamera');

async function getSnapshot(lprCamera) {
    return new Promise(async (resolve, reject) => {
        let response;
        let requestData = {
            api: `/GetSnapshot`
        }
        response = await httpLpr.makeRequest(lprCamera, requestData, ` get snapshot request: ${lprCamera.Name} `);
        let buff = Buffer.from(response.data).toString('base64');
        let data = `data:image/jpeg;base64,${buff}`;
        resolve(data);
    })
}

module.exports.getSnapshot = getSnapshot;