const httpLpr = require('../http/httpWithLprCamera');
const settings = require('./../../../../settings.json');
const snap = require('../actions/getSnapshot');
async function sendOpenGateRequest(lprCamera) {
    return new Promise(async (resolve, reject) => {
        let response;
        let requestData = {
            body: `<?xml version="1.0" encoding="UTF-8"?>` +
                `<config version="1.0" xmlns="http://www.ipc.com/ver10">` +
                `<action>` +
                `<status>true</status>` +
                `</action>` +
                `</config>`,
            api: `/ManualAlarmOut`
        }
        response = await httpLpr.makeRequest(lprCamera, requestData, ` open gate request: ${lprCamera.Name} `);
        resolve(response);
    })
}

async function sendCloseGateRequest(lprCamera) {
    let response;
    let requestData = {
        body: `<?xml version="1.0" encoding="UTF-8"?>` +
            `<config version="1.0" xmlns="http://www.ipc.com/ver10">` +
            `<action>` +
            `<status>false</status>` +
            `</action>` +
            `</config>`,
        api: `/ManualAlarmOut`
    }

    response = await httpLpr.makeRequest(lprCamera, requestData, ` close gate request: `);
    return response;
}


async function openGateByTimerConfiguration(lprCamera) {
    let openGateResponse = await sendOpenGateRequest(lprCamera);
    let response = {
        snapshot: await snap.getSnapshot(lprCamera)
    };
    setTimeout(function () {
        sendCloseGateRequest(lprCamera);
    }, settings.closeGateAfterTime * 1000);
    response["status"] = (openGateResponse.status == 200) ? true : false;
    return response;
}

module.exports.openGateByTimerConfiguration = openGateByTimerConfiguration;