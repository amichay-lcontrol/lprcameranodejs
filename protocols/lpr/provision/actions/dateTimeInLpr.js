const log = require("../../../../global/console");
const httpLpr = require("../http/httpWithLprCamera");
const globalFunction = require("../../../../global/globalFunction");
const convertDateFormat = require('../../../../global/date');
const keepAlivingWithLpr = require('./../alive/keepAlivingWithLpr');
const convert = require('xml-js');
const axios = require('axios');
const convert_option = {
    compact: true,
    spaces: 4
}

//update date time in lpr camera
async function updateDateTimeInCamera(lprCamera) {
    var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + "/GetDateAndTime";

    return new Promise(async function (resolve, reject) {
        try {
            log.msg("get date time from lpr camera: " + lprCamera.Name);
            var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

            var response = await axios.get(url, {
                headers: {
                    Authorization: basicAuth
                }
            });

            if (response.status == 200) {

                let date = new Date();
                let currentTime = convertDateFormat(date, "Date", 'yyyy-MM-dd HH:mm:ss');

                let jsonResponse = convert.xml2json(response.data, convert_option);
                jsonResponse = JSON.parse(jsonResponse);

                let localTimeInCamera = jsonResponse["config"]["time"]["synchronizeInfo"]["currentTime"]["_cdata"];
                let timeInCamera = new Date(localTimeInCamera).getTime();
                var dateTimeNow = new Date().getTime();

                if (timeInCamera != dateTimeNow) {
                    jsonResponse["config"]["time"]["synchronizeInfo"]["type"]["_text"] = "manually";
                    jsonResponse["config"]["time"]["synchronizeInfo"]["currentTime"]["_cdata"] = `${currentTime}`;

                    let xml = convert.js2xml(jsonResponse, convert_option);
                    url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + "/SetDateAndTime";
                    var response = await axios.post(url, xml, {
                        headers: {
                            Authorization: basicAuth
                        }
                    });
                    if (response.status == 200) {
                        log.info("Succesfuly update date time in lpr camera: " + lprCamera.Name);
                        return resolve(true);
                    } else {
                        log.error("Cannot update date time in lpr camera: " + lprCamera.Name);
                        return resolve(false);
                    }
                }
            }
        } catch (ex) {
            log.error("Error in update date time in lpr camera: " + lprCamera.Name + ", " + ex.message);
            return resolve(false);
        }
    });
}

module.exports.updateDateTimeInCamera = updateDateTimeInCamera;