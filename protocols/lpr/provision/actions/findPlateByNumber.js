const httpLpr = require('../http/httpWithLprCamera');
const convert = require('xml-js');
const log = require("../../../../global/console");

const convert_option = {
    compact: true,
    spaces: 4
}

// Add vehicle plate
async function findPlateByNumber(lprCamera, plateNumber) {
    return new Promise(async (resolve, reject) => {
        let isFound;
        let searchCounter = 0;
        var plate;
        while (!isFound && searchCounter < 5) {
            let requestData = {
                body: `<?xml version=\"1.0\" encoding=\"utf-8\"?>` +
                    `<config xmlns=\"http://www.ipc.com/ver10\" version=\"1.7\">` +
                    `<vehiclePlates type=\"list\" maxCount=\"10000\" count=\"1000\">` +
                    `<searchFilter>` +
                    `<carPlateNum type=\"string\">${plateNumber}</carPlateNum>` +
                    `</searchFilter>` +
                    `</vehiclePlates>` +
                    `</config>`,
                api: `/GetVehiclePlate`
            }
            response = await httpLpr.makeRequest(lprCamera, requestData, ` search plate: ${plateNumber} in lpr camera: `);
            if (response && response.status == 200) {
                let jsonResponse = convert.xml2json(response.data, convert_option);
                jsonResponse = JSON.parse(jsonResponse);
                let att = jsonResponse["config"]["vehiclePlates"]["_attributes"]["count"];

                if (att == "1") {
                    plate = {
                        id: jsonResponse["config"]["vehiclePlates"]["item"]['keyId']["_text"],
                        allowlistValue: jsonResponse["config"]["vehiclePlates"]["item"]["plateItemType"]["_text"]
                    }
                    break;
                }
            }
            searchCounter++;
        }
        resolve(plate);
    });
}

//get all car in the lpr camer
function findAllPlateNumber(lprCamera) {
    return new Promise(async (resolve, reject) => {
        try {
            let requestData = {
                body: `<?xml version=\"1.0\" encoding=\"utf-8\"?>` +
                    `<config>` +
                    `<searchFilter>` +
                    `<pageIndex type="unit32">0</pageIndex>` +
                    `<pageSize>10</pageSize>` +
                    `<listType>allList</listType>` +
                    `<carPlateNum></carPlateNum> ` +
                    `</searchFilter>` +
                    `</config>`,
                api: `/GetVehiclePlate`
            }
            response = await httpLpr.makeRequest(lprCamera, requestData, `get all plates in lpr camera: `);
            let jsonResponse;

            //if response data
            if (response && response.status == 200) {
                jsonResponse = convert.xml2json(response.data, convert_option);
                jsonResponse = JSON.parse(jsonResponse);
            } else
                jsonResponse = false;

            resolve(jsonResponse);
        } catch (ex) {
            log.error("error when try get all the car list in lpr camera: " + lprCamera.Name + ", Error: " + ex.message)
            reject(ex);
        }
    });
}

module.exports.findPlateByNumber = findPlateByNumber;
module.exports.findAllPlateNumber = findAllPlateNumber;