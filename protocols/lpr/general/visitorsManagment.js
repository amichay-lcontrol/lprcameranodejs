const fs = require('fs');
const log = require("../../../global/console");
const programControl = require("../../../program-control");
const lprHikvisionDownloadWhiteList = require("../hikvision/actions/downloadWhiteList");
const lprHikvisionRemoveFromList = require("../hikvision/actions/removeFromList");
const lprProvisionDownloadWhiteList = require("../provision/actions/downloadWhiteList");
const lprProvisionRemoveFromList = require("../provision/actions/removePlate");



//this function to add a new visitor to he list of visitors
function updatVisitor(visitorData) {

    try {
        //read all visitors from visitors file 
        let visitors = fs.readFileSync("visitors.txt");
        visitors = JSON.parse(visitors);

        //check if this visitor car number already exist so just update the dates of him
        let visitorExist = false;
        for (let i = 0; i < visitors.needsToEnter.length; i++) {
            if (visitors.needsToEnter[i].CarNumber == visitorData.CarNumber) {
                visitorExist = true;
                visitors.needsToEnter[i].FromDate = visitorData.FromDate;
                visitors.needsToEnter[i].ToDate = visitorData.ToDate;
            }
        }
        //add the new visitor to the list 
        if (!visitorExist)
            visitors.needsToEnter.push(visitorData);

        //Filter the array by date
        // filterarrayByDate(visitors.needsToAdd);

        //update the file after changes
        fs.writeFileSync("visitors.txt", JSON.stringify(visitors));

        return true;
    } catch (ex) {
        log.error("error when try update visitor, Error: " + ex.message)
        return false;
    }
}

//filter the event by the date
function filterArrayByDate(array) {
    let arr = [];

    arr = array[0];
    for (let row of array) {}

    /*    array.filter(row => {
           if()
       }) */
}


//this interval function
//that check if arriver time to update car number of visiotor
//allowed it in the lpr camera, or remove it fromm lpr camera
async function checkForNeedsUpdateInVisitorData() {
    let interval = 30000;

    let updateVisitorData = setInterval(async () => {
        try {
            //read all visitors from visitors file 
            let visitors = fs.readFileSync("visitors.txt");
            visitors = JSON.parse(visitors);

            //read all lpr cameras
            let lprCameras = programControl.lprCameras;

            let i = 0;

            //          allowed car number          //
            //check if there is visiotr that need enter 
            for (let visitor of visitors.needsToEnter) {

                //if the time of the visitor is arrived
                if (new Date(visitor.FromDate).getTime() <= new Date().getTime()) {

                    //allowed this visiotr in the lpr camera
                    for (let lprCamera of lprCameras) {
                        //HIKVISION
                        if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                            let response = await lprHikvisionDownloadWhiteList.downloadWhiteList(lprCamera, [visitor.CarNumber]);

                            if (response && response.status) {
                                log.msg("Succesfully added visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to add visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);
                        }

                        //PROVISION
                        if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                            let response = await lprProvisionDownloadWhiteList.addPlateToWhiteList(lprCamera, [visitor.CarNumber]);

                            if (response && response[0].status) {
                                log.msg("Succesfully added visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to add visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);
                        }
                    }


                    //after update the visitor car number in the lpr cameras
                    //we remove it from visitor.NeedToEneter
                    visitors.needsToEnter.splice(i, 1);
                    //we move it to visitor.needToExit
                    visitors.needsToExit.push(visitor);

                    //update visitors file
                    fs.writeFileSync("visitors.txt", JSON.stringify(visitors));
                    i--;
                }
                i++;
            }

            //also check that the visitors that in the array of nnedToExit are allowed in the lpr camera
            /* for (let visitor of visitors.needsToExit) {
                //if the time of the visitor is still to allowed in the parking
                if (new Date(visitor.FromDate).getTime() <= new Date().getTime()) {

                    //allowed this visiotr in the lpr camera
                    for (let lprCamera of lprCameras) {
                        //HIKVISION
                        if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                            let response = await lprHikvisionDownloadWhiteList.downloadWhiteList(lprCamera, [visitor.CarNumber]);

                            if (response && response[0].status) {
                                log.msg("Succesfully update visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to update visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);
                        }

                        //PROVISION
                        if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                            let response = await lprProvisionDownloadWhiteList.addPlateToWhiteList(lprCamera, [visitor.CarNumber]);

                            if (response && response[0].status) {
                                log.msg("Succesfully update visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to update visiotr car number: " + visitor.CarNumber + ", to lpr camera: " + lprCamera.Name);
                        }
                    }
                }
            } */




            i = 0;
            //          Remove car number          //
            //check if there is visiotr that need exit 
            for (let visitor of visitors.needsToExit) {
                //if the time of the visitor is finish
                //so remove this visiotr from the lpr camera
                if (new Date(visitor.ToDate).getTime() <= new Date().getTime()) {

                    //remove this visiotr from the lpr camera
                    for (let lprCamera of lprCameras) {
                        //HIKVISION
                        if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                            let response = await lprHikvisionRemoveFromList.removeFromList(lprCamera, [visitor.CarNumber]);

                            if (response) {
                                log.msg("Succesfully remove visiotr car number: " + visitor.CarNumber + ", from lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to remove visiotr car number: " + visitor.CarNumber + ", from lpr camera: " + lprCamera.Name);
                        }

                        //PROVISION
                        if (lprCamera.TypeId == 2 && lprCamera["IsAlive"]) {
                            let response = await lprProvisionRemoveFromList.removePlate(lprCamera, visitor.CarNumber);

                            if (response) {
                                log.msg("Succesfully remove visiotr car number: " + visitor.CarNumber + ", from lpr camera: " + lprCamera.Name);

                            } else
                                log.error("Failed to remove visiotr car number: " + visitor.CarNumber + ", from lpr camera: " + lprCamera.Name);
                        }
                    }


                    //after update the visitor car number in the lpr cameras
                    //we remove it from visitor.NeedToExit
                    visitors.needsToExit.splice(i, 1);

                    //update visitors file
                    fs.writeFileSync("visitors.txt", JSON.stringify(visitors));
                    i--;
                }
                i++;
            }
        } catch (ex) {
            log.error("error when try update visitor data, Error: " + ex.message);
        }
    }, interval);
}

module.exports.updatVisitor = updatVisitor;
module.exports.checkForNeedsUpdateInVisitorData = checkForNeedsUpdateInVisitorData;