const http = require('../../../http/http');
const log = require("../../../global/console");
const fs = require('fs');
const programControl = require('../../../program-control');

const downloadWhiteListHikvision = require("../hikvision/actions/downloadWhiteList");
const getAndUpdateCarListHikvision = require("../hikvision/actions/getAndUpdateCarList");
const removeFromListHikvision = require("../hikvision/actions/removeFromList");

const downloadWhiteListProvision = require("../provision/actions/downloadWhiteList");
const findPlateByNumberProvision = require("../provision/actions/findPlateByNumber");
const removeFromListProvision = require("../provision/actions/removePlate");



//this function will return false(when the parking not full, or this user have a private parking space)
//and true if this is not allowed(when parking full, or not have private parking space)
function checkIfCompanyFullParkingSpace(carNumber) {
    return new Promise(async (resolve, reject) => {
        //get all the users
        let users = fs.readFileSync("users.txt");
        users = JSON.parse(users);

        //get all the visitors
        let visitors = fs.readFileSync("visitors.txt");
        visitors = JSON.parse(visitors);

        //get user by car number 
        let userData = users.filter((user) => user.CarNumber == carNumber)[0];

        //if no found this car number in users, search it in visitors 
        if (!userData) {
            userData = visitors.needsToEnter.filter((visitor) => visitor.CarNumber == carNumber)[0];

            //if no found in need to eneter search in need to exit
            if (!userData) {
                userData = visitors.needsToExit.filter((visitor) => visitor.CarNumber == carNumber)[0];
            }
        }

        //check if found this user data
        if (userData) {
            //check if this user not have private parking space 
            if (!userData.ReservedParkingId) {

                //get the exist car list from the file
                let existingCarData = fs.readFileSync("company_parking_count.txt");
                existingCarData = JSON.parse(existingCarData);

                //check if this company full parking
                if (existingCarData[userData.CompanyId]) {
                    if (existingCarData[userData.CompanyId].ParkingCount == 0) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                } else {
                    log.normal("This company not exist parking space, CompanyId: " + userData.CompanyId);
                    resolve(true);
                }
            } else {
                resolve(false);
            }
        } else {
            log.error("this user car number: " + carNumber + " not found in the camera or in the users.txt");
            //try to update users list from DB
            //let response = await programControl.getAndUpdateDataFromDB();

            //call again to the function
            //return checkIfCompanyFullParkingSpace(carNumber);
        }
    })
}


//return all the existing car in the parking now by the companyId
function getParkingManagement(data) {
    //get the exist car list from the file
    let existingCarData = fs.readFileSync("company_parking_count.txt");
    existingCarData = JSON.parse(existingCarData);

    let response = {};
    if (data.companyId == "*") {
        for (let key of Object.keys(existingCarData))
            response[key] = {
                cars: existingCarData[key].existingCars,
                occupiedParkingSpace: existingCarData[key].existingCars.length
            }
    } else {
        response[data.companyId] = {
            cars: existingCarData[data.companyId].existingCars,
            occupiedParkingSpace: existingCarData[data.companyId].existingCars.length
        }
    }

    return response;
}


//when enterd a car we add it to the list of cars existing by company 
function updateExistingCars(event, lprCamera) {
    let users;
    let existingCarData;

    // reading file  //
    try {
        //get the exist car list from the file
        existingCarData = fs.readFileSync("company_parking_count.txt");
        existingCarData = JSON.parse(existingCarData);

        //get all the users
        users = fs.readFileSync("users.txt");
        users = JSON.parse(users);
    } catch (ex) {
        log.error("Error when try read file. Error:" + ex.message);
    }


    //get the user data of this car number
    let foundInUser = false;
    for (let user of users) {
        if (event.carNumber == user.CarNumber) {
            event["userIndex"] = user.UserIndex;
            event["companyId"] = user.CompanyId;
            event["reservedParkingId"] = user.ReservedParkingId;

            foundInUser = true;
        }
    }

    //if not found in user check it in visitors
    if (!foundInUser) {
        //get all the visitors
        visitors = fs.readFileSync("visitors.txt");
        visitors = JSON.parse(visitors);

        for (let visitor of visitors.needsToExit) {
            if (event.carNumber == visitor.CarNumber) {
                event["userIndex"] = visitor.UserIndex;
                event["companyId"] = visitor.CompanyId;
                event["reservedParkingId"] = null;
                event["visitor"] = true;
            }
        }
    }


    //in case of car go inside => (camare IN)
    if (lprCamera.IN_OUT == 0) {
        try {
            //check that this car not in the list already
            let existsInList = existingCarData[event["companyId"]] ? existingCarData[event["companyId"]].existingCars.filter((i) => i.CarNumber == event.carNumber).length : false;
            if (!existsInList) {

                if (event.companyId && existingCarData[event.companyId]) {
                    //add this user event to the array by the company 
                    existingCarData[event.companyId].existingCars.push({
                        UserIndex: event.userIndex,
                        CarNumber: event.carNumber,
                        ReservedParkingId: event.reservedParkingId,
                        Visitor: event.visitor ? event.visitor : false
                    })

                    //in case of this user not have parking space
                    if (event.ReservedParkingId == null) {
                        existingCarData[event.companyId].ParkingCount--;

                        //check if finish the avalibale parking for this company 
                        if (existingCarData[event.companyId].ParkingCount <= 0) {

                            //update the white list car in the camera
                            updateWhiteList(event.companyId, users, existingCarData, "Remove");
                        }
                    }
                } else {
                    log.error("This user car not found the list of users, carNumber: " + event.carNumber);
                }
            }
        } catch (ex) {
            log.error("Error when try update car that go inside, Error: " + ex.message);
        }
    }


    //in case of car go out => (camare OUT)
    else if (lprCamera.IN_OUT == 1) {
        try {

            //clear this user car from existing car 
            for (let i = 0; i < existingCarData[event.companyId].existingCars.length; i++) {
                if (event.carNumber == existingCarData[event.companyId].existingCars[i].CarNumber &&
                    event.userIndex == existingCarData[event.companyId].existingCars[i].UserIndex)
                    existingCarData[event.companyId].existingCars.splice(i, 1);
            }

            //the car that go out now make one place empty
            if (event.ReservedParkingId == null) {
                //check if there is no avalibale parking
                if (existingCarData[event.companyId].ParkingCount >= 0) {
                    existingCarData[event.companyId].ParkingCount++;

                    //update the white list car in the camera
                    updateWhiteList(event.companyId, users, existingCarData, "Add");
                } else
                    existingCarData[event.companyId].ParkingCount++;
            }
        } catch (ex) {
            log.error("Error when try update car that go outside, Error: " + ex.message);
        }
    }

    //in case of car go in/out => (camare IN and OUT)
    //check if this car already inside => take it out  else add it to parking
    else if (lprCamera.IN_OUT == 2) {

        //check if this car inside 
        let existsInList = existingCarData[event["companyId"]] ? existingCarData[event["companyId"]].existingCars.filter((i) => i.CarNumber == event.carNumber).length : false;

        //if this car not exist add him
        if (!existsInList) {
            try {
                if (event.companyId && existingCarData[event.companyId]) {
                    //add this user event to the array by the company 
                    existingCarData[event.companyId].existingCars.push({
                        UserIndex: event.userIndex,
                        CarNumber: event.carNumber,
                        ReservedParkingId: event.reservedParkingId,
                        Visitor: event.visitor ? event.visitor : false
                    })

                    //in case of this user not have private parking space
                    if (event.ReservedParkingId == null) {
                        existingCarData[event.companyId].ParkingCount--;

                        //check if finish the avalibale parking for this company 
                        if (existingCarData[event.companyId].ParkingCount <= 0) {

                            //update the white list car in the camera
                            updateWhiteList(event.companyId, users, existingCarData, "Remove");
                        }
                    }
                } else {
                    log.error("This user car not found the list of users, carNumber: " + event.carNumber);
                }
            } catch (ex) {
                log.error("Error when try update car that go inside, Error: " + ex.message);
            }
        }

        //if this car already inside take him out
        else {
            try {
                //clear this user car from existing car 
                for (let i = 0; i < existingCarData[event.companyId].existingCars.length; i++) {
                    if (event.carNumber == existingCarData[event.companyId].existingCars[i].CarNumber &&
                        event.userIndex == existingCarData[event.companyId].existingCars[i].UserIndex)
                        existingCarData[event.companyId].existingCars.splice(i, 1);
                }

                //the car that go out now make one place empty
                //in case of not have private parking space
                if (event.ReservedParkingId == null) {
                    existingCarData[event.companyId].ParkingCount++;

                    //check if there is no avalibale parking
                    if (existingCarData[event.companyId].ParkingCount >= 0) {

                        //update the white list car in the camera
                        updateWhiteList(event.companyId, users, existingCarData, "Add");
                    }
                }
            } catch (ex) {
                log.error("Error when try update car that go outside, Error: " + ex.message);
            }
        }
    }


    //save existing car in the file
    fs.writeFileSync("company_parking_count.txt", JSON.stringify(existingCarData));
}


//this function will update the white list in the lpr camera 
//we get the users list and check for cars that not in the parking in this time 
//and by the action add them to the camera or remove them from camera
function updateWhiteList(companyId, users, existingCarData, action) {
    //get all the car that we need update in white list
    let cars = users.filter((user) => user.CompanyId == companyId && user.ReservedParkingId == null);
    for (let i = 0; i < cars.length; i++)
        for (let existCar of existingCarData[companyId].existingCars)
            if (cars[i].CarNumber == existCar.CarNumber) {
                cars.splice(i, 1);
                i--;
                break;
            }

    //we get all the cars that thay are not in the parking right now
    let carList = cars.map(car => car.CarNumber);

    //run for all the cameras
    //and remove from them the car list
    let lprCameras = programControl.lprCameras;

    for (let lprCamera of lprCameras) {
        //do change in camara, only in => IN camera
        //and in camera of In & OUT
        if (lprCamera.IN_OUT == 0 || lprCamera.IN_OUT == 2) {

            //check if camera ia alive
            /*    if (lprCamera.IsAlive) {
               } else {
                   log.error("Can`t update white list in lpr camera: : " + lprCamera.Name, +", camera are no connected");
               } */

            if (action == "Add") {
                if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"])
                    downloadWhiteListHikvision.downloadWhiteList(lprCamera, carList);
                else if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"])
                    downloadWhiteListProvision.addPlateToWhiteList(lprCamera, carList);
            } else if (action == "Remove") {
                if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"])
                    removeFromListHikvision.removeFromList(lprCamera, carList);
                else if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"])
                    removeFromListProvision.removeFromPlatesFromList(lprCamera, carList);
            }
        } else {
            log.error("lpr camera are not connected, lpr camera: " + lprCamera.Name);
        }
    }
}


///this function get a car number and lpr camera, and check if this car allowed in this camera
function checkCarNumberStatusInLprCamera(lprCamera, carNumber) {
    return new Promise(async (resolve, reject) => {
        try {
            //get all the users
            let users = fs.readFileSync("users.txt");
            users = JSON.parse(users);

            let car = {
                carNumber: carNumber,
                status: null
            };


            //Hikvision
            if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                //get the car list in this lpr camera
                let carListInLprCamera = await getAndUpdateCarListHikvision.getCarListFromLprCamera(lprCamera);

                //if this car number found in the white list of the camera,   status => 0
                if (carListInLprCamera.filter((row) => row["Plate Num"] == carNumber).length > 0) {
                    car.status = 0;

                    log.info("car number: " + carNumber + " found as allowed car, in lpr camera: " + lprCamera.Name);
                }

                //if this car found in the users list is mean that he allowed in the lpr but at this moment  the parking group spaces are full,    status => 1
                else if (users.filter((car) => car.CarNumber == carNumber).length > 0) {
                    car.status = 1;
                }

                //if this car number not allowed in this lpr camera,   status => 2
                else
                    car.status = 2;
            }

            //Provision
            else if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                //if this car number found in the white list of the camera,   status => 0
                let plate = await findPlateByNumberProvision.findPlateByNumber(lprCamera, carNumber);
                if (plate && plate.allowlistValue == "whiteList") {
                    car.status = 0;

                    log.info("car number: " + carNumber + " found as allowed car, in lpr camera: " + lprCamera.Name);
                }

                //if this car found in the users list is mean that he allowed in the lpr but at this moment  the parking group spaces are full,    status => 1
                else if (users.filter((car) => car.CarNumber == carNumber).length > 0) {
                    car.status = 1;
                }

                //if this car number not allowed in this lpr camera,   status => 2
                else
                    car.status = 2;
            }

            resolve(car);
        } catch (ex) {
            log.error("Error in check car number status, Error: " + ex.message);
            reject(ex);
        }
    })
}


//this function get list of cars to remove them from parking
//relese the occupied parking of them 
function removeOccupiedParking(data) {
    let users;
    let existingCarData;

    // reading file  //
    try {
        //get the exist car list from the file
        existingCarData = fs.readFileSync("company_parking_count.txt");
        existingCarData = JSON.parse(existingCarData);

        //get all the users
        users = fs.readFileSync("users.txt");
        users = JSON.parse(users);
    } catch (ex) {
        log.error("Error when try read file. Error:" + ex.message);
    }


    try {
        //clear this user car from existing car 
        for (let i = 0; i < existingCarData[data.companyId].existingCars.length; i++) {
            if (data.carNumber == existingCarData[data.companyId].existingCars[i].CarNumber &&
                data.userIndex == existingCarData[data.companyId].existingCars[i].UserIndex) {
                existingCarData[data.companyId].existingCars.splice(i, 1);
                break;
            }
        }

        //the car that go out now make one place empty
        if (data.ReservedParkingId == null) {
            existingCarData[data.companyId].ParkingCount++;

            //check if there is no avalibale parking
            if (existingCarData[data.companyId].ParkingCount < 5) {
                //update the white list car in the camera
                updateWhiteList(data.companyId, users, existingCarData, "Add");
            }
        }

        //save existing car in the file
        fs.writeFileSync("company_parking_count.txt", JSON.stringify(existingCarData));
    } catch (ex) {
        log.error("Error when try update car that go outside, Error: " + ex.message);
    }



    //update main on change in occupied parking space
    let parkingData = {};
    parkingData[data.companyId] = {
        cars: existingCarData[data.companyId].existingCars,
        occupiedParkingSpace: existingCarData[data.companyId].existingCars.length
    }
    try {
        let response = http.updateOccupiedParkingSpace(parkingData);
    } catch (ex) {
        log.error("error when try update main service on update in occupied parking space, Error: " + ex.message)
    }
}



// update the white list 
// this function will check if there is company that have parking places,
// but some useres are not in the white list so we add them
// or if exist car in white list that not need to be, so we remove them

async function updateAllowedCarsInParkingManagment() {

    //get the exist car list from the file
    let existingCarData = fs.readFileSync("company_parking_count.txt");
    existingCarData = JSON.parse(existingCarData);

    //get all the users
    let users = fs.readFileSync("users.txt");
    users = JSON.parse(users);

    log.msg("get action to - update all cars number in the lpr cameras");
    //get all company ids
    let compayIds = Object.keys(existingCarData);

    ///run for all the company
    for (let companyId of compayIds) {

        //check if this company have parking spaces avalibale
        if (existingCarData[companyId].ParkingCount > 0) {

            //check if exist car in the list that not need to be, so remove it
            checkCarNumberNeedToRemove();

            //update all the user of this company
            updateWhiteList(companyId, users, existingCarData, "Add");
        }
    }
}

// check if exist car number in the lpr camera that not exist in the users list
// so this car number was need to delete from the lpr camera list
async function checkCarNumberNeedToRemove() {
    try {
        log.msg("this action to remove car number from lpr camera");
        let carList = [];

        //get lpr cameras
        let lprCameras = programControl.lprCameras;

        for (let lprCamera of lprCameras) {
            carList = [];

            //HIKVISION
            if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                //get the car in this lpr camera
                let carList = await getAndUpdateCarListHikvision.getCarListFromLprCamera(lprCamera);

                //check if this car number exist in users list
                carList = checkCarNumberInUsers(carList);

                //remove car list
                if (carList.length > 0)
                    await removeFromListHikvision.removeFromList(lprCamera, carList)
            }

            //PROVISION
            if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                //get the car in this lpr camera
                let response = await findPlateByNumberProvision.findAllPlateNumber(lprCamera);

                //found some car in the list
                if (response && response["config"]["vehiclePlates"]["_attributes"]["count"] > 0) {
                    if (response["config"]["vehiclePlates"]["_attributes"]["count"] == 1)
                        carList.push(response["config"]["vehiclePlates"]["item"]["carPlateNumber"]["_cdata"]);
                    else
                        for (let car of response["config"]["vehiclePlates"]["item"])
                            carList.push(car["carPlateNumber"]["_cdata"]);

                    //check if this car number exist in users list
                    carList = checkCarNumberInUsers(carList);

                    //remove car list
                    if (carList.length > 0)
                        await removeFromListProvision.removeFromPlatesFromList(lprCamera, carList);
                } else
                    log.error("not response data from lpr camera, Or not found car list, in lpr camera :" + lprCamera.Name)
            }
        }

    } catch (ex) {
        log.error("error when try remove some car number that needs from lpr camera, Error:" + ex.message)
    }
}

//get car list from lpr and check for every car number that he also in the users list,visitors list,blscklist
function checkCarNumberInUsers(carList) {
    //get users
    let users = fs.readFileSync("users.txt");
    users = JSON.parse(users);

    //get visitors
    let visitors = fs.readFileSync("visitors.txt");
    visitors = JSON.parse(visitors);

    //get black list

    //run for all car list
    for (let i = 0; i < carList.length; i++) {

        //if this car number exist in the user list so take it out is mean that this car number allowed in the lpr camera
        if (users.filter((user) => user.CarNumber == carList[i]).length > 0) {
            carList.splice(i, 1);
            i--;
        }

        //also check if this car number is visitor
        else if (visitors.needsToExit.filter((visitor) => visitor.CarNumber == carList[i]).length > 0) {
            carList.splice(i, 1);
            i--;
        }

        //also check if this car number is in black list
    }

    return carList;
}

module.exports.checkIfCompanyFullParkingSpace = checkIfCompanyFullParkingSpace;
module.exports.getParkingManagement = getParkingManagement;
module.exports.updateExistingCars = updateExistingCars;
module.exports.checkCarNumberStatusInLprCamera = checkCarNumberStatusInLprCamera;
module.exports.removeOccupiedParking = removeOccupiedParking;
module.exports.updateAllowedCarsInParkingManagment = updateAllowedCarsInParkingManagment;
module.exports.checkCarNumberNeedToRemove = checkCarNumberNeedToRemove;