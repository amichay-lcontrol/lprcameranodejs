const httpLpr = require("../http/httpWithLprCamera");
const globalFunction = require("../../../../global/globalFunction");
const log = require("../../../../global/console");
const dateTimeInLpr = require("../actions/dateTimeInLpr");
const stremingWithLpr = require("../events/stremingWithLpr");

const keepAlivingWithLpr = require("./keepAlivingWithLpr")




//get alive for lpr camera
async function getCamerasAlive(lprCamera) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/System/deviceInfo", "try get alive from lpr camera: ");

        if (response && response.status == 200) {
            log.info(lprCamera.Name + ": this camera is alive");
            lprCamera["IsAlive"] = true;
            lprCamera["IsAliveNeedUpdate"] = true;

            //check if this is LPR camera
            getLprCamera(lprCamera);

        } else {
            log.error("Camera : " + lprCamera.Name + ", not connected")
            lprCamera["IsAlive"] = false;

            //try connect again to the lpr camera
            getCamerasAlive(lprCamera);
        }

    } catch (ex) {
        log.error("Error in camera : " + lprCamera.Name + "; " + ex.message);
        lprCamera["IsAlive"] = false;

        //try connect again to the lpr camera
        getCamerasAlive(lprCamera);
    }
}



//check if this is LPR camera
async function getLprCamera(lprCamera) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/ITC/capability", "check if this is lpr camera: ");

        if (response && response.status == 200) {
            let jsonResponse = globalFunction.convertXmlToJson(response.data);

            let support = (jsonResponse["ITCCap"]["isSupportVehicleDetection"]["_text"] == 'true');

            if (support) {
                log.info(lprCamera.Name + ": this camera is LPR,")
                lprCamera['IsLpr'] = true;

                //update date time in camera
                await dateTimeInLpr.updateDateTimeInCamera(lprCamera);

                //start listinner to event from LPR cameras
                await stremingWithLpr.listeningToEvent(lprCamera);

            } else {
                log.error(lprCamera.Name + ": this camera is not LPR,")
                lprCamera['IsLpr'] = false;
                keepAlivingWithLpr.checkingAliveForNotLprCameras(lprCamera);
            }
        } else {
            log.error(lprCamera.Name + ": this camera is not LPR,")
            lprCamera['IsLpr'] = false;
        }

    } catch (error) {
        lprCamera['IsLpr'] = false;
        log.error(lprCamera.Name + ": Error to get capability of this LPR, " + error.message)
    }
}


module.exports.getCamerasAlive = getCamerasAlive;