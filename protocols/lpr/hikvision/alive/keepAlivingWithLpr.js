const connectionWithLpr = require("./connectionWithLpr");
const log = require("../../../../global/console");
const http = require("../../../../http/http");
const convertDateFormat = require("../../../../global/date");
const httpLpr = require("../http/httpWithLprCamera");
const stremingWithLpr = require("../events/stremingWithLpr");





/*******************************/
/***** timeout - for alive *****/
/*******************************/
//this function clear the lprTimeout and keep alive for the LPR camera
function clearAliveLprTimeout(lprCamera) {
    if (lprCamera["IsAliveNeedUpdate"]) {
        lprCamera["IsAlive"] = true;
        lprCamera["IsAliveNeedUpdate"] = false;

        //send message to the main service that the connection start 
        updateingLprCamera(lprCamera);
    }

    //clean time out
    clearTimeout(lprCamera["ConnectionTimeout"]);
    lprCamera["ConnectionTimeout"] = null;
}

//we add for this lprCamera Timeout
function activeTimeout(lprCamera) {

    //start Timeout
    lprCamera["ConnectionTimeout"] = setTimeout(async () => {

        //check if camera is alive
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/System/deviceInfo", "try get alive from lpr camera: ");
        if (response && response.status == 200)
            lprCamera["IsAlive"] = true;
        else
            lprCamera["IsAlive"] = false;


        if (lprCamera["IsAlive"]) {
            log.info("we not get response from lpr camera, so we chack alive, lpr camera : " + lprCamera.Name + " is alive.");

            //check if memory stream is open,if no close it and open the socket again
            if (!lprCamera["memStream"].readable) {
                //close the socket connection
                log.info("The strem with this lpr camera was close, we open new one, with lpr camera: " + lprCamera.Name);
                lprCamera["memStream"].end();
            } 

            // if say the socket is open, we will check after X time if still not get data in the socket we close it and open again
            else {
                lprCamera["ConnectionTimeout"] = setTimeout(() => {
                    activeTimeout(lprCamera);

                    log.info("we close socket and open again,  lpr camera : " + lprCamera.Name + " is alive.");
                    //close socket ans start again
                    lprCamera["memStream"].end();

                }, 5000)
            }

        } else {
            lprCamera["IsAliveNeedUpdate"] = true;
            lprCamera['StremingData'] = "";

            //close the socket connection
            log.info("we will close the strem , for open new one, lpr camera: " + lprCamera.Name);
            lprCamera["memStream"].end();

            //send message to the main service thet the connection stop
            updateingLprCamera(lprCamera);

            //try connect again to the lpr camera
            connectionWithLpr.getCamerasAlive(lprCamera);
        }
    }, 60000);
}

//updateing lpr camera alive
function updateingLprCamera(lprCamera) {
    if (lprCamera.IsAlive)
        log.info(lprCamera.Name + ": Connect again ");
    else
        log.error(lprCamera.Name + ": Disconnect ");

    let dataToSend = {
        LprCameraId: lprCamera.CameraId,
        IsAlive: lprCamera.IsAlive,
        EventTime: convertDateFormat(new Date(), 'Date', 'yyyy-MM-dd HH:mm:ss')
    }
    let response = http.lprCameraAlive(dataToSend);

    if (response && response) {
        //smile
    } else {
        //try send again
    }
}




/**************************************/
// checking alive for not lpr cameras //
/**************************************/
async function checkingAliveForNotLprCameras(lprCamera) {

    var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/System/deviceInfo", "try get alive from camera: ");

    if (response && response.status == 200) {
        if (!lprCamera["lastState"]) {
            log.info(lprCamera.Name + ": this camera is alive again");
            lprCamera["IsAlive"] = true;
            lprCamera["lastState"] = true;

            //send message to the main service thet the connection start
            updateingLprCamera(lprCamera);
        }
        setTimeout(() => {
            checkingAliveForNotLprCameras(lprCamera)
        }, 5000);
    } else {
        if (lprCamera["lastState"]) {
            log.error(lprCamera.Name + ": this camera is not alive");
            lprCamera["IsAlive"] = false;
            lprCamera["lastState"] = false;

            //send message to the main service thet the connection start
            updateingLprCamera(lprCamera);
        }
        checkingAliveForNotLprCameras(lprCamera);
    }
}

module.exports.clearAliveLprTimeout = clearAliveLprTimeout;
module.exports.activeTimeout = activeTimeout;
module.exports.checkingAliveForNotLprCameras = checkingAliveForNotLprCameras;