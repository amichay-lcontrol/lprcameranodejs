const httpLpr = require("../http/httpWithLprCamera");
const globalFunction = require("../../../../global/globalFunction");
const convertDateFormat = require('../../../../global/date');
const log = require("../../../../global/console");






//update date time in lpr camera
async function updateDateTimeInCamera(lprCamera) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/System/time", "get date time from lpr camera: ");

        if (response.status == 200) {
            let jsonResponse = globalFunction.convertXmlToJson(response.data);

            let localTimeInCamera = jsonResponse["Time"]["localTime"]["_text"];
            let timeInCamera = new Date(localTimeInCamera).getTime();

            //we take the time now
            var dateTimeNow = new Date().getTime();

            //check if the time in camera is not valid , update it
            if (timeInCamera != dateTimeNow) {
                let timeZone = jsonResponse["Time"]["timeZone"]["_text"];
                let localtimeToUpdate = convertDateFormat(new Date(), "Date", "yyyy-MM-ddThh:mm:ss+hh:mm", timeZone);
                jsonResponse["Time"]["localTime"]["_text"] = localtimeToUpdate;

                //update the time in camera
                let xml = globalFunction.convertJsonToXml(jsonResponse);

                try {
                    var response = await httpLpr.putHttpResponse(lprCamera, "/ISAPI/System/time", "update date time in lpr camera: ", xml);

                    if (response && response.status == 200) {
                        log.info("Succesfuly update date time in lpr camera: " + lprCamera.Name);
                    } else {
                        log.error("Canot update date time in lpr camera: " + lprCamera.Name);
                    }
                } catch (ex) {
                    log.error("Error in update date time in lpr camera: " + lprCamera.Name + ", " + ex.message);
                }
            } else {
                log.info("The time in camera is updated, in lpr camera: " + lprCamera.Name);
            }
        } else {
            log.error("Canot get date time in lpr camera: " + lprCamera.Name);
        }
    } catch (ex) {
        log.error("Error in get date time in lpr camera: " + lprCamera.Name + ", " + ex.message);
    }
}

module.exports.updateDateTimeInCamera = updateDateTimeInCamera;