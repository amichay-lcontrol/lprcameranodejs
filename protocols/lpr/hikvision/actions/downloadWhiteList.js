const getAndUpdateCarList = require("./getAndUpdateCarList");
const programControl = require("../../../../program-control");
const lprParkingManagement = require("../../general/parkingManagement");
const log = require("../../../../global/console");
const fs = require('fs');




//this function get alist of allowed cars and download them to the lpr camera
async function downloadWhiteList(lprCamera, carList) {
    try {

        //update users and company from DB
        //await programControl.getAndUpdateDataFromDB();

        let response = [];
        //get the car list in this lpr camera
        let carListInLprCamera = await getAndUpdateCarList.getCarListFromLprCamera(lprCamera);

        //check if return data
        if (carListInLprCamera != null) {
            // --update the list by the car list that we get--
            for (let car of carList) {

                //check if this car belong to company that the parking space of her is full 
                let fullParkingOrNotPrivateParkingSpace = await lprParkingManagement.checkIfCompanyFullParkingSpace(car);

                if (!fullParkingOrNotPrivateParkingSpace) {
                    // check if this number already in the list,
                    // we just update it to allowed car
                    if (carListInLprCamera.filter((row) => row["Plate Num"] == car).length > 0) {
                        for (let row of carListInLprCamera) {
                            if (row["Plate Num"] == car) {
                                log.msg("Update car number: -" + car + "- to white list in lpr camera: " + lprCamera.Name);
                                row["Group(0 black list, 1 white list)"] = "1";
                                break;
                            }
                        }
                    }
                    //we add this car number to the list as allowed car
                    else {
                        log.msg("Add car number: -" + car + "- to white list lpr camera: " + lprCamera.Name);
                        /*  lprCamera["insercCarsNum"]++;
                         log.msg(lprCamera["insercCarsNum"] + ",  cars addedd to the lpr camera"); */
                        carListInLprCamera.push({
                            "No.": (carListInLprCamera.length + 1).toString(),
                            "Plate Num": car,
                            "Group(0 black list, 1 white list)": "1"
                        })
                    }
                }

                response.push({
                    status: true,
                    car: car
                })
            }

            //download all the car useres to the camera
            if (!await getAndUpdateCarList.updateListInLprCamera(lprCamera, carListInLprCamera)) {

                for (let row of response) {
                    row.status = false;
                }

                throw (false);
            }
        }

        return response;
    } catch (ex) {
        log.error("Error when download white list, Error: " + ex.message);
        return false;
    }
}

module.exports.downloadWhiteList = downloadWhiteList;