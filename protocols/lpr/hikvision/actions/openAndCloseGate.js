const httpLpr = require("../http/httpWithLprCamera");
const log = require("../../../../global/console");
const settings = require("../../../../settings.json");




//open gate in lpr camera
async function openGate(lprCamera) {
    try {
        var postData = '<IOPortData version="2.0" xmlns="http://www.isapi.org/ver20/XMLSchema"><outputState>high</outputState></IOPortData>';
        var response = await httpLpr.putHttpResponse(lprCamera, "/ISAPI/System/IO/outputs/1/trigger", "Try open gate in camera: ", postData);

        if (response && response.status == 200) {
            log.info("Open gate succesfully in lpr camera:" + lprCamera.Name);

            //take snapshot
            let snapshot = "data:image/jpeg;base64," + await getSnapshotFromLprCamera(lprCamera);

            let response = {
                status: true,
                snapshot: snapshot
            };

            //close the gate after time
            closeGate(lprCamera);

            return response;
        } else {
            log.error("Canot open gate in lpr camare:" + lprCamera.Name);
            let response = {
                status: false,
                snapshot: null
            };
            return response;
        }
    } catch (ex) {
        log.error("Error in lpr camare:" + lprCamera.Name + ", when try open gate, Error:" + ex.message);
        let response = {
            status: false,
            snapshot: null
        };
        return response;
    }
}


//close gate in lpr camera
async function closeGate(lprCamera) {
    try {
        setTimeout(async () => {
            var postData = '<IOPortData version="2.0" xmlns="http://www.isapi.org/ver20/XMLSchema"><outputState>low</outputState></IOPortData>';
            var response = await httpLpr.putHttpResponse(lprCamera, "/ISAPI/System/IO/outputs/1/trigger", "Try close gate in camera: ", postData);


            if (response.status == 200) {
                log.info("Close gate succesfully in lpr camera:" + lprCamera.Name + ", after: " + settings.closeGateAfterTime + " secounds.");

            } else {
                log.error("Canot close gate in lpr camare:" + lprCamera.Name);
                return false;
            }
        }, settings.closeGateAfterTime * 1000);

    } catch (ex) {
        log.error("Error in lpr camare:" + lprCamera.Name + ", when try close gate, Error:" + ex.message);
        return false;
    }
}

//get snapshot from the lpr camera
async function getSnapshotFromLprCamera(lprCamera, imgId) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/Streaming/channels/1/picture", "Try get snapshot from lpr: ", true);

        if (response && response.status == 200) {

            base64data = Buffer.from(response.data).toString('base64');

            return base64data;
        } else {
            log.error("Canot get snapshot, from lpr camera: " + lprCamera.Name)
            return null;
        }
    } catch (ex) {
        log.error("Error when get snapshot, from lpr camera: " + lprCamera.Name + ", " + ex.message)
        return null;
    }
}



module.exports.openGate = openGate;