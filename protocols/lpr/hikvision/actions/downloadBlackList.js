const getAndUpdateCarList = require("./getAndUpdateCarList");


//this function get alist of not allowed cars and download them as a black list to the lpr camera
async function downloadBlackList(lprCamera, carList) {

    try {
        let response = [];

        //get the car list in this lpr camera
        let carListInLprCamera = await getAndUpdateCarList.getCarListFromLprCamera(lprCamera);

        //check if return data
        if (carListInLprCamera != null) {
            // --update the list by the car list that we get--
            for (let car of carList) {
                // check if this number already in the list,
                // we just update it to not allowed car
                if (carListInLprCamera.filter((row) => row["Plate Num"] == car).length > 0) {
                    for (let row of carListInLprCamera) {
                        if (row["Plate Num"] == car) {
                            row["Group(0 black list, 1 white list)"] = "0";
                            break;
                        }
                    }
                }
                //we add this car number to the list as allowed car
                else {
                    carListInLprCamera.push({
                        "No.": (carListInLprCamera.length + 1).toString(),
                        "Plate Num": car,
                        "Group(0 black list, 1 white list)": "0"
                    })
                }

                response.push({
                    status: true,
                    car: car
                })
            }


            if (!await getAndUpdateCarList.updateListInLprCamera(lprCamera, carListInLprCamera)) {
                for (let row of response) {
                    row.status = false;
                }
                throw (false);
            }
        }


        return response;
    } catch (ex) {
        return response;
    }

}

module.exports.downloadBlackList = downloadBlackList;