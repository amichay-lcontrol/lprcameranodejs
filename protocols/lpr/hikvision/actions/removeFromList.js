const getAndUpdateCarList = require("./getAndUpdateCarList");
const log = require("../../../../global/console");


//this function get alist of cars to remove them from the lpr camera
async function removeFromList(lprCamera, carList) {
    let response = [];

    //get the car list in this lpr camera
    let carListInLprCamera = await getAndUpdateCarList.getCarListFromLprCamera(lprCamera);

    try {
        //check if return data
        if (carListInLprCamera != null) {
            // --update the list by the car list that we get--
            for (let car of carList) {
                for (let i = 0; i < carListInLprCamera.length; i++) {

                    // check if this number already in the list,
                    // we will remove it from the list
                    if (carListInLprCamera[i]["Plate Num"] == car) {
                        carListInLprCamera.splice(i, 1);
                        break;
                    }
                }

                response.push({
                    status: true,
                    car: car
                })
            }
            getAndUpdateCarList.updateListInLprCamera(lprCamera, carListInLprCamera);
        }

        return response;
    } catch (ex) {
        log.error("Error happend when try to remove cars from lpr camera:" + lprCamera.name + ", Error: " + ex.message);
        return response;
    }

}


module.exports.removeFromList = removeFromList