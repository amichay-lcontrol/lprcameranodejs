const httpLpr = require("../http/httpWithLprCamera")
const XLSX = require('xlsx');
const fs = require('fs');
const log = require("../../../../global/console");







//get the car list from the lpr camera
async function getCarListFromLprCamera(lprCamera) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/Traffic/channels/1/licensePlateAuditData", "Try get car list from camera: ", true);

        if (response.status == 200) {
            /* convert data to binary string */
            var data = response.data;

            var arr = new Array();
            for (var i = 0; i != data.length; ++i)
                arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");

            /* Call XLSX */
            var workbook = XLSX.read(bstr, {
                type: "binary"
            });

            //write the file
            //XLSX.writeFile(workbook, "carNumbersAllowedFromLPRCamera.xls");

            /* get the work sheet name */
            var first_sheet_name = workbook.SheetNames[0];

            /* Get worksheet */
            var worksheet = workbook.Sheets[first_sheet_name];
            let xlsData = XLSX.utils.sheet_to_json(worksheet, {
                raw: true
            })

            return xlsData;
        } else {
            log.error("Canot get the car list from lpr camera: " + lprCamera.Name);
            return null;
        }
    } catch (ex) {
        log.error("Error in get the car list from lpr camera: " + lprCamera.Name + ", " + ex.message);
        return null;
    }
}

//update the list in lpr camera
async function updateListInLprCamera(lprCamera, carListToLprCamera) {
    try {
        if (carListToLprCamera.length == 0)
            addTitleToEmptyList(carListToLprCamera);

        let wb = XLSX.utils.book_new();
        let ws = XLSX.utils.json_to_sheet(carListToLprCamera);
        XLSX.utils.book_append_sheet(wb, ws, 'first');

        XLSX.writeFile(wb, "carNumbersAllowed_" + lprCamera.CameraId + ".xls");

        let fileData = fs.readFileSync("carNumbersAllowed_" + lprCamera.CameraId + ".xls").toString('hex');
        let workBookHex = []
        for (var i = 0; i < fileData.length; i += 2)
            workBookHex.push('0x' + fileData[i] + '' + fileData[i + 1])

        let workBookBinary = Buffer.from(workBookHex);

        var response = await httpLpr.putHttpResponse(lprCamera, "/ISAPI/Traffic/channels/1/licensePlateAuditData", "Try update car list to camera: ", workBookBinary);

        if (response && response.status == 200) {
            //let jsonResponse = convert.xml2json(response.data, convert_option);
            log.info("Update succesfuly the car list in lpr camera: " + lprCamera.Name);

            return true;
        } else {
            log.error("Canot update the car list in lpr camera: " + lprCamera.Name);
            return false;
        }
    } catch (ex) {
        log.error("Error in update the car list in lpr camera: " + lprCamera.Name + ", " + ex.message);
        return false;
    }
}

//
function addTitleToEmptyList(carListInLprCamera) {
    carListInLprCamera.push({
        "No.": "0",
        "Plate Num": "0",
        "Group(0 black list, 1 white list)": "0"
    })
}

module.exports.getCarListFromLprCamera = getCarListFromLprCamera;
module.exports.updateListInLprCamera = updateListInLprCamera;