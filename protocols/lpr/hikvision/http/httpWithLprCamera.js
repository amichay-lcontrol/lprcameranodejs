const axios = require("axios");
const log = require("../../../../global/console");
const stremingWithLpr = require("../events/stremingWithLpr");
const keepAlivingWithLpr = require("../alive/keepAlivingWithLpr");

//GET
// this function will make the http get request with lpr camera
async function getHttpResponse(lprCamera, api, actionMessage, getItAssArrayBuffer, sendXmlData) {

    var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + api;
    try {
        log.msg(actionMessage + lprCamera.Name)
        var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

        if (getItAssArrayBuffer)
            if (sendXmlData)
                var response = await axios.get(url, {
                    responseType: 'arraybuffer',
                    headers: {
                        Authorization: basicAuth,
                        'Accept': 'application/binary'
                    },
                    data: sendXmlData
                });
            else
                var response = await axios.get(url, {
                    responseType: 'arraybuffer',
                    headers: {
                        Authorization: basicAuth,
                        'Accept': 'application/binary'
                    }
                });
        else
            var response = await axios.get(url, {
                headers: {
                    Authorization: basicAuth
                }
            });
        return response;

    } catch (ex) {
        log.error("Error when try get http request on camera: " + lprCamera.Name + ", Error:" + ex.message)
    }
}


//GET
// this function will make the http get request with lpr camera
async function getHttpStreamResponse(lprCamera, api, actionMessage) {

    var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + api;
    try {
        log.msg(actionMessage + lprCamera.Name)
        let basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

        const CancelToken = axios.CancelToken;
        let cancel;
        let response;

        await axios({
            url: url,
            method: 'GET',
            responseType: 'stream',
            headers: {
                Authorization: basicAuth,
                Ip: lprCamera.Ip
            },
            cancelToken: new CancelToken(function executor(c) {
                cancel = c;
            })
        }).then((res) => {
            res.cancel = cancel;
            response = res;
        });


        return response;
    } catch (ex) {
        log.error("Error when try get http stream request on camera: " + lprCamera.Name + ", Error:" + ex.message);

        //clear the timeout
        keepAlivingWithLpr.clearAliveLprTimeout(lprCamera);

        //close all the stream if open
        if (lprCamera["memStream"])
            lprCamera["memStream"].end();

        setTimeout(async () => {
            //try connect again to the stream of camera
            await stremingWithLpr.listeningToEvent(lprCamera);
        }, 5000)
    }
}


//PUT
// this function will make the http put request with lpr camera
async function putHttpResponse(lprCamera, api, actionMessage, data) {

    var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + api;
    try {
        log.msg(actionMessage + lprCamera.Name)
        var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

        var response = await axios.put(url, data, {
            headers: {
                Authorization: basicAuth
            }
        });
        return response;

    } catch (ex) {
        log.error("Error when try http put request on camera: " + lprCamera.Name + ", Error: " + ex.message)
    }
}


//POST
// this function will make the http psot request with lpr camera
async function postHttpResponse(lprCamera, api, actionMessage, data) {

    var url = "http://" + lprCamera.Ip + ":" + lprCamera.Port + api;

    try {
        log.msg(actionMessage + lprCamera.Name)
        var basicAuth = 'Basic ' + Buffer.from(lprCamera.UserName + ':' + lprCamera.Password).toString('base64');

        var response = await axios.post(url, data, {
            headers: {
                Authorization: basicAuth
            }
        });
        return response;

    } catch (ex) {
        log.error("Error when try http post request on camera: " + lprCamera.Name + ", Error:" + ex.message)
    }
}



module.exports.getHttpResponse = getHttpResponse;
module.exports.getHttpStreamResponse = getHttpStreamResponse;
module.exports.putHttpResponse = putHttpResponse;
module.exports.postHttpResponse = postHttpResponse;