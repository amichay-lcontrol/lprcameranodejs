const http = require("../../../../http/http");
const httpLpr = require("../http/httpWithLprCamera");
const log = require("../../../../global/console");
const globalFunction = require("../../../../global/globalFunction");
const convertDateFormat = require('../../../../global/date');
const keepAlivingWithLpr = require("../alive/keepAlivingWithLpr");
const settings = require("../../../../settings.json");
const parkingManagement = require("../../general/parkingManagement");
const fs = require('fs');

var lastEventTime = new Date();
var sentEvents = [];
var tempEvents = [];
var newEventInterval;




//new event except
async function newEvent(lprCamera, eventName) {
    //check the event type
    switch (eventName) {
        case "vehicledetection": {
            //get from the lpr camera the event of new car detact
            var events = await getTheCarDetection(lprCamera);

            //send the events to the main service
            for (let event of events) {
                //check if this event not send yet
                if (checkInTempEvents(event, 0)) {
                    //
                    tempEvents.push(event);

                    //get snapshot from lpr camera
                    let snapshot = await getSnapshotFromLprCamera(lprCamera);
                    log.info("Take snapshot from lpr camera");

                    //get the image of event from lpr camera
                    event["eventImage"] = "data:image/jpeg;base64," + await getImageFromLprCamera(lprCamera, event.imgId);
                    log.info("Take the image from lpr camera");

                    //get big image from lpr camera
                    let image = await getBigImageFromLprCamera(lprCamera, event, 0);
                    //if no success we add the snapshot
                    if (image == null)
                        image = snapshot;
                    else
                        log.info("Get big image from lpr camera");
                    event["snapshot"] = "data:image/jpeg;base64," + image;

                    //push the event to the array that save all the event that we detect
                    event["lprCamera"] = lprCamera;

                    //check again that not exist other evnet like this
                    if (checkInTempEvents(event, 1)) {
                        sentEvents.push(event);
                        log.info("Push new event to array of sentEvents");
                    }
                }
            }

            break;
        }
        case "movedetection": {
            log.info('Get new event of:' + eventName + ', from LPR camera: ' + lprCamera.Name);


            break;
        }
        case "videoloss": {

            //clear the timeOut, and active the lpr to alive
            //keepAlivingWithLpr.clearAliveLprTimeout(lprCamera);

            // active timeOut that will check in 1 secounds if we alive or not
            //if this timeOut will be alive is mean that we not get other sign from the LPR camer
            //and we stop the alive of the LPR camera
            //keepAlivingWithLpr.activeTimeout(lprCamera);

            break;
        }
        case "VMD": {

            //clear the timeOut, and active the lpr to alive
            //keepAlivingWithLpr.clearAliveLprTimeout(lprCamera);

            // active timeOut that will check in 1 secounds if we alive or not
            //if this timeOut will be alive is mean that we not get other sign from the LPR camer
            //and we stop the alive of the LPR camera
            //keepAlivingWithLpr.activeTimeout(lprCamera);

            break;
        }
        default: {
            /*  log.info('******************************');
             log.info('Get new event of:' + eventName + ', from LPR camera: ' + lprCamera.Name);
             log.info('******************************'); */
            break;
        }
    }
}

//get from the camera the last car thet detaction
async function getTheCarDetection(lprCamera) {
    try {
        var postData = "<AfterTime version='2.0' xmlns='http://www.isapi.org/ver20/XMLSchema'><captureTime>" + 0 + "</captureTime></AfterTime>";
        var response = await httpLpr.postHttpResponse(lprCamera, "/ISAPI/Traffic/channels/1/vehicleDetect/plates", "Get the car detection from lpr: ", postData);

        if (response.status == 200) {
            let events = [];
            let jsonResponse = globalFunction.convertXmlToJson(response.data);
            let platesArr = jsonResponse["Plates"]["Plate"];

            //if we get noe plates 
            if (platesArr) {
                try {
                    //take only the plate that we not send before
                    for (let i = 0; i < platesArr.length; i++) {
                        //we cut all the plate that the time pf them less then the time of the last event thet we send
                        let eventDate = convertDateFormat(platesArr[i]["captureTime"]["_text"], "yyyyMMddTHHmmss+0200", "Date");
                        if (eventDate.getTime() <= new Date(lastEventTime).getTime()) {
                            platesArr.splice(i, 1);
                            i--;
                        } else {
                            //
                            lastEventTime = eventDate;
                        }
                    }
                } catch (ex) {
                    log.error("Error in lpr camera: " + lprCamera.Name + ", canot read data from platesArr, Error: " + ex.message)
                }


                //send all the event that heppand after time
                try {
                    if (platesArr && platesArr.length)
                        for (let plate of platesArr) {
                            let event = {
                                serialNumber: settings.serialNumber,
                                lprCameraId: lprCamera.CameraId,
                                eventTime: convertDateFormat(plate["captureTime"]["_text"], "yyyyMMddTHHmmss+0200", "yyyy-MM-dd HH:mm:ss"),
                                eventType: plate["matchingResult"]["_text"] == "blacklist" ? 0 : plate["matchingResult"]["_text"] == "whitelist" ? 1 : 2,
                                carNumber: plate["plateNumber"]["_text"],
                                direction: plate["direction"]["_text"],
                                imgId: plate["picName"]["_text"],
                                eventAcceptedInTime: convertDateFormat(new Date(), "Date", "yyyy-MM-dd HH:mm:ss")
                            };
                            events.push(event);
                        }
                } catch (ex) {
                    log.error("erorr when update events from platesarr, Error: " + ex.message);
                }
            } else {
                log.error("platesArr is null");
            }

            return events;
        } else {
            log.error('Canot get the last events in LPR camera :' + lprCamera.Name);
            return [];
        }
    } catch (error) {
        log.error('Error in LPR camera :' + lprCamera.Name + ", cannot get the last events," + error.message);
        return [];
    }
}

//get image from the lpr camera by imgId
async function getImageFromLprCamera(lprCamera, imgId) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/doc/ui/images/plate/" + imgId + ".jpg", "Try get image from lpr: ", true);

        if (response.status == 200) {

            base64data = Buffer.from(response.data).toString('base64');

            return base64data;
        } else {
            log.error("Canot get img, imgId: " + imgId + "from lpr camera: " + lprCamera.Name)
            return null;
        }
    } catch (ex) {
        log.error("Error when get img, imgId: " + imgId + "from lpr camera: " + lprCamera.Name + ", " + ex.message)
        return null;
    }
}


//get the full image from camera
/////
async function getBigImageFromLprCamera(lprCamera, event, attemptCount) {
    try {
        let startDate = convertDateFormat(event.eventTime, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-ddThh:mm:ssZ", null, "-"); //minus 5 minuted
        let endDate = convertDateFormat(event.eventTime, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-ddThh:mm:ssZ", null, "+"); //plus 5 minutes
        log.msg("Event in time: " + event.eventTime + ",;  search event in time: " + startDate);

        //post 
        let postData = `<?xml version: "1.0" encoding="utf-8"?><CMSearchDescription><searchID>C8CEDE8F-ECF0-0001-CF44-33C016C05070</searchID>
                        <trackIDList><trackID>103</trackID></trackIDList>
                        <timeSpanList><timeSpan><startTime>${startDate}</startTime><endTime>${endDate}</endTime></timeSpan></timeSpanList>
                        <contentTypeList><contentType>metadata</contentType></contentTypeList><maxResults>50</maxResults><searchResultPostion>0</searchResultPostion>
                        <metadataList><metadataDescriptor>//recordType.meta.std-cgi.com/vehicleDetection</metadataDescriptor><SearchProperity><plateSearchMask/>
                        <stateOrProvince>255</stateOrProvince></SearchProperity></metadataList></CMSearchDescription>`
        var response = await httpLpr.postHttpResponse(lprCamera, "/ISAPI/ContentMgmt/search", "Try get big image info from lpr: ", postData);

        if (response && response.status == 200) {

            try {
                let jsonResponse = globalFunction.convertXmlToJson(response.data);

                if (Number(jsonResponse["CMSearchResult"]["numOfMatches"]["_text"]) > 0) {
                    let playbackURI;

                    //if this is list of data
                    if (Array.isArray(jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"])) {
                        let carFound = false;


                        //run for all the car that response
                        for (let i = 0; i < jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"].length; i++) {
                            playbackURI = jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"][i]["mediaSegmentDescriptor"]["playbackURI"]["_text"];

                            //check if this image is of this event 
                            //check by car number
                            let toPosition = playbackURI.search("&size=");
                            let fromPosition = playbackURI.search("&size=") - event.carNumber.length;
                            let imageOfCarNumber = playbackURI.substring(fromPosition, toPosition);

                            //check by car number && by the date
                            if (imageOfCarNumber == event.carNumber) {
                                carFound = true;
                                //    Fix bug    //
                                //this function fix bug of them
                                //add to the string the text:    'amp;', after every '&'
                                for (let i = 0; i < playbackURI.length; i++)
                                    if (playbackURI[i] == "&")
                                        playbackURI = [playbackURI.slice(0, i + 1), "amp;", playbackURI.slice(i + 1)].join('');
                                //    Fix bug    //


                                //get 
                                let xmlData = `<downloadRequest version='1.0' xmlns='http://”http://www.isapi.org/ver20/XMLSchema'>
                                       <playbackURI>${playbackURI}</playbackURI>
                                       </downloadRequest>`
                                var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/ContentMgmt/download", "Try get big image from lpr: ", true, xmlData);
                                break;
                            }
                        }

                        //if car not fount call again to bring data
                        if (!carFound) {
                            if (attemptCount < 5) {
                                //cal again to this function
                                await sleep(2000);
                                log.normal("the car number does not match, Call function again to bring the big image, lprCamera: " + lprCamera.Name)
                                attemptCount++
                                return await getBigImageFromLprCamera(lprCamera, event, attemptCount);
                            } else return null;
                        }
                    }
                    //in case of one row data in the list
                    else {
                        playbackURI = jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"]["mediaSegmentDescriptor"]["playbackURI"]["_text"];


                        //check if this image is of this event 
                        let toPosition = playbackURI.search("&size=");
                        let fromPosition = playbackURI.search("&size=") - event.carNumber.length;
                        let imageOfCarNumber = playbackURI.substring(fromPosition, toPosition);

                        //check by car number
                        if (imageOfCarNumber == event.carNumber) {

                            //    Fix bug    //
                            //this function fix bug of them
                            //add to the string the text:    'amp;', after every '&'
                            for (let i = 0; i < playbackURI.length; i++)
                                if (playbackURI[i] == "&")
                                    playbackURI = [playbackURI.slice(0, i + 1), "amp;", playbackURI.slice(i + 1)].join('');
                            //    Fix bug    //


                            //get the image by the id of her
                            let xmlData = `<downloadRequest version='1.0' xmlns='http://”http://www.isapi.org/ver20/XMLSchema'>
                                       <playbackURI>${playbackURI}</playbackURI>
                                       </downloadRequest>`
                            var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/ContentMgmt/download", "Try get big image from lpr: ", true, xmlData);
                        }

                        //if is not this cae number call again to bring the list
                        else {
                            if (attemptCount < 5) {
                                //cal again to this function
                                await sleep(500);
                                log.normal("the car number does not match, Call function again to bring the big image, lprCamera: " + lprCamera.Name)
                                attemptCount++
                                return await getBigImageFromLprCamera(lprCamera, event, attemptCount);
                            } else return null;
                        }
                    }
                } else {
                    if (attemptCount < 5) {
                        //cal again to this function
                        await sleep(500);
                        log.normal("not found image in the list,Call function again to bring the big image, lprCamera: " + lprCamera.Name)
                        attemptCount++;
                        return await getBigImageFromLprCamera(lprCamera, event, attemptCount);
                    } else return null;
                }

                if (response && response.status == 200) {
                    base64data = Buffer.from(response.data).toString('base64');

                    return base64data;
                } else {
                    log.error("Canot get big image, from lpr camera: " + lprCamera.Name)
                    return null;
                }
            } catch (ex) {
                log.error("Canot get the big image, Error: " + ex.message);
                return null
            }
        } else {
            log.error("Canot get the list of big image, lpr camera name:" + lprCamera.Name);
            return null;
        }
    } catch (ex) {
        log.error("Canot get the list of big image from lpr camera ,Error: " + ex.message);
        return null;
    }
}

/* async function getBigImageFromLprCamera(lprCamera, event, attemptCount) {
    try {
        let startDate = convertDateFormat(event.eventTime, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-ddThh:mm:ssZ", null, "-"); //minus 5 Hours
        let endDate = convertDateFormat(event.eventTime, "yyyy-MM-dd HH:mm:ss", "yyyy-mm-ddThh:mm:ssZ", null, "+"); //plus 5 Hours

        //post 
        let postData = `<?xml version: "1.0" encoding="utf-8"?><CMSearchDescription><searchID>C8CEDE8F-ECF0-0001-CF44-33C016C05070</searchID>
                        <trackIDList><trackID>103</trackID></trackIDList>
                        <timeSpanList><timeSpan><startTime>${startDate}</startTime><endTime>${endDate}</endTime></timeSpan></timeSpanList>
                        <contentTypeList><contentType>metadata</contentType></contentTypeList><maxResults>50</maxResults><searchResultPostion>0</searchResultPostion>
                        <metadataList><metadataDescriptor>//recordType.meta.std-cgi.com/vehicleDetection</metadataDescriptor><SearchProperity><plateSearchMask/>
                        <stateOrProvince>255</stateOrProvince></SearchProperity></metadataList></CMSearchDescription>`
        var response = await httpLpr.postHttpResponse(lprCamera, "/ISAPI/ContentMgmt/search", "Try get big image info from lpr: ", postData);

        if (response && response.status == 200) {

            try {
                let jsonResponse = globalFunction.convertXmlToJson(response.data);

                if (Number(jsonResponse["CMSearchResult"]["numOfMatches"]["_text"]) > 0) {
                    let playbackURI;
                    if (Array.isArray(jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"])) {
                        let len = jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"].length;
                        playbackURI = jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"][len - 1]["mediaSegmentDescriptor"]["playbackURI"]["_text"];
                    } else
                        playbackURI = jsonResponse["CMSearchResult"]["matchList"]["searchMatchItem"]["mediaSegmentDescriptor"]["playbackURI"]["_text"];

                    //check if this image is of this event 
                    //check by car number
                    let toPosition = playbackURI.search("&size=");
                    let fromPosition = playbackURI.search("&size=") - event.carNumber.length;
                    let imageOfCarNumber = playbackURI.substring(fromPosition, toPosition);
                    if (imageOfCarNumber == event.carNumber) {

                        //    Fix bug    //
                        //this function fix bug of them
                        //add to the string the text:    'amp;', after every '&'
                        for (let i = 0; i < playbackURI.length; i++)
                            if (playbackURI[i] == "&")
                                playbackURI = [playbackURI.slice(0, i + 1), "amp;", playbackURI.slice(i + 1)].join('');
                        //    Fix bug    //


                        //get 
                        let xmlData = `<downloadRequest version='1.0' xmlns='http://”http://www.isapi.org/ver20/XMLSchema'>
                                   <playbackURI>${playbackURI}</playbackURI>
                                   </downloadRequest>`
                        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/ContentMgmt/download", "Try get big image from lpr: ", true, xmlData);
                    } else {
                        if (attemptCount < 5) {
                            //cal again to this function
                            await sleep(500);
                            log.normal("the car number does not match, Call function again to bring the big image, lprCamera: " + lprCamera.Name)
                            attemptCount++
                            return await getBigImageFromLprCamera(lprCamera, event, attemptCount);
                        } else return null;
                    }
                } else {
                    if (attemptCount < 5) {
                        //cal again to this function
                        await sleep(500);
                        log.normal("not found image in the list,Call function again to bring the big image, lprCamera: " + lprCamera.Name)
                        attemptCount++;
                        return await getBigImageFromLprCamera(lprCamera, event, attemptCount);
                    } else return null;
                }

                if (response && response.status == 200) {
                    base64data = Buffer.from(response.data).toString('base64');

                    return base64data;
                } else {
                    log.error("Canot get big image, from lpr camera: " + lprCamera.Name)
                    return null;
                }
            } catch (ex) {
                log.error("Canot get the big image, Error: " + ex.message);
                return null
            }
        } else {
            log.error("Canot get the list of big image, lpr camera name:" + lprCamera.Name);
            return null;
        }
    } catch (ex) {
        log.error("Canot get the list of big image from lpr camera ,Error: " + ex.message);
        return null;
    }
}
 */

function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

//get snapshot from the lpr camera
async function getSnapshotFromLprCamera(lprCamera, imgId) {
    try {
        var response = await httpLpr.getHttpResponse(lprCamera, "/ISAPI/Streaming/channels/1/picture", "Try get snapshot from lpr: ", true);

        if (response && response.status == 200) {

            base64data = Buffer.from(response.data).toString('base64');

            return base64data;
        } else {
            log.error("Canot get snapshot, from lpr camera: " + lprCamera.Name)
            return null;
        }
    } catch (ex) {
        log.error("Error when get snapshot, from lpr camera: " + lprCamera.Name + ", " + ex.message)
        return null;
    }
}


//this function check if this event not sent already to the main service
//false => if this event alrady sent before
//true =? this is a new event
function checkInTempEvents(event, counterLimit) {
    let counter = 0;

    for (let i = 0; i < tempEvents.length; i++) {
        let oldEvent = tempEvents[i];

        if (oldEvent.carNumber == event.carNumber &&
            (Math.abs((new Date(oldEvent.eventTime).getTime() - new Date(event.eventTime).getTime()) / 1000) < 20)) {
            counter++;
        }
    }

    return counter <= counterLimit;
}


//this interval running for every 500 milisecounds and check if there is a new events 
//if we found new event we send it to the main
async function checkingForNewEvents() {
    //newEventInterval = setInterval(async () => {
    let obj = {};

    //if there is a new events
    if (sentEvents.length > 0) {
        //check if there duplicate events
        let len = sentEvents.length;
        for (let i = 0; i < len; i++)
            obj[sentEvents[i]['carNumber']] = sentEvents[i];

        //clean this events from the array
        sentEvents.splice(0, len);
    }

    //we send all the event that left as a new events
    for (let key of Object.keys(obj)) {
        let event = obj[key];
        event.eventAcceptedInTime = convertDateFormat(new Date(), "Date", "yyyy-MM-dd HH:mm:ss");

        //send the event to the main service
        let lprCamera = event["lprCamera"];
        delete(event["lprCamera"]);


        log.specialMsg(" ***** New event *****")
        log.specialMsg("Car number - " + event.carNumber);
        log.specialMsg("Date - " + event.eventTime);
        log.specialMsg("Arrived in time - " + event.eventAcceptedInTime);
        if (event.eventType == 1)
            log.specialMsg("Allowed car - this car in white list");
        else if (event.eventType == 0)
            log.specialMsg("Not allowed car - this car in black list");
        else if (event.eventType == 2)
            log.specialMsg("Not allowed car - this Unknown car");
        log.specialMsg(` ***** ${lprCamera.Name} ***** `)

        log.info('Succesfully update the latest event, from LPR camera: ' + lprCamera.Name);
        lastEventTime = event["eventTime"];
        //update main on new event
        try {
            let response = await http.updateNewLprCameraEvent(event);
        } catch (ex) {
            log.error("Error when try sending event to main service, Error: " + ex.message)
        }

        
        /* ****************************************** */
        //     this system for manage the parking     //
        /* ****************************************** */
        //check if the hikvision camera is the meneger of the parking
        //HOKVISION
        if (settings.camManager == 1) {
            //if new allowed car entered, save this car in the list of existing cars in the parking
            if (event.eventType == 1) {
                try {
                    await parkingManagement.updateExistingCars(event, lprCamera);

                    //update parking state in main
                    let parkingData = {};
                    //get the exist car list from the file
                    let existingCarData = fs.readFileSync("company_parking_count.txt");
                    existingCarData = JSON.parse(existingCarData);

                    //check if found company for this car number
                    if (event.companyId) {
                        if (existingCarData[event.companyId].existingCars)
                            parkingData[event.companyId] = {
                                cars: existingCarData[event.companyId].existingCars,
                                occupiedParkingSpace: existingCarData[event.companyId].existingCars.length
                            }

                        //update main om change in  occupied parking space
                        try {
                            let response = await http.updateOccupiedParkingSpace(parkingData);
                        } catch (ex) {
                            log.error("error when try update main service on update in occupied parking space, Error: " + ex.message)
                        }
                    } else {
                        log.error("Error this is allowed car number but not found in list of users, carNumber: " + event.carNumber);
                    }
                } catch (ex) {
                    log.error("Error when try update in existing car, Error: " + ex.message);
                }
            }
        }
    }

    setTimeout(checkingForNewEvents, 500);
    // }, 500)
}
checkingForNewEvents();


//this interval running in every 1 minute check for event that older the 1 minutes and remove them from the array
function cleanOldEvents() {
    setInterval(() => {
        try {
            // log.normal("tempEvent array (Before): " + tempEvents.length);
            //  log.normal("sentEvents array (Before): " + sentEvents.length);
            //clean tempEvent
            for (let i = 0; i < tempEvents.length; i++) {
                if ((new Date().getTime() - new Date(tempEvents[i].eventTime).getTime()) / 60000 > 1) {
                    tempEvents.splice(i, 1);
                    i--;
                }
            }

            //clean sentEvent
            for (let i = 0; i < sentEvents.length; i++) {
                if ((new Date().getTime() - new Date(sentEvents[i].eventTime).getTime()) / 60000 > 1) {
                    sentEvents.splice(i, 1);
                    i--;
                }
            }

            //log.normal("tempEvent array (After): " + tempEvents.length);
            //log.normal("sentEvents array (After): " + sentEvents.length);
        } catch (ex) {
            log.error("Canot remove event from sentEvents, Error:" + ex.message);
        }
    }, 1000 * 60)
}
cleanOldEvents();



module.exports.newEvent = newEvent;