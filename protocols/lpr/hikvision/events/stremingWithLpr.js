const httpLpr = require("../http/httpWithLprCamera");
const globalFunction = require("../../../../global/globalFunction");
const eventFromLpr = require("./eventFromLpr");
const log = require("../../../../global/console");
const MemoryStream = require('memorystream');
const keepAlivingWithLpr = require("../alive/keepAlivingWithLpr");





//start lisining to events from the camera
async function listeningToEvent(lprCamera) {
    try {
        let response = await httpLpr.getHttpStreamResponse(lprCamera, "/ISAPI/Event/notification/alertStream", "Try start listing to event from camera: ");

        if (response && response.status == 200) {
            log.info(lprCamera.Name + ": start listing events from LPR camera");
            lprCamera['InStreaming'] = true;

            let memStream = new MemoryStream();
            response.data.pipe(memStream);

            lprCamera["memStream"] = memStream;

            //when get new event from this lpr camera
            memStream.on('data', async function (data) {
                //log.info("###############  " + lprCamera.Name);
                lprCamera['StremingData'] += data;

                //clear the timeOut, and active the lpr to alive
                keepAlivingWithLpr.clearAliveLprTimeout(lprCamera);

                // active timeOut that will check in 1 secounds if we alive or not
                //if this timeOut will be alive is mean that we not get other sign from the LPR camer
                //and we stop the alive of the LPR camera
                keepAlivingWithLpr.activeTimeout(lprCamera);

                //check if in the strem there is a new event
                checkStreamData(lprCamera);
            });


            //when finish the strem from lpr camera
            memStream.on('end', async function (flag) {
                log.error('End stream with LPR camera :' + lprCamera.Name);
                lprCamera['InStreaming'] = false;
                response.cancel();
                memStream.destroy();

                /*   if (lprCamera["IsAlive"])
                      //try connect again to the camera
                      await listeningToEvent(lprCamera); */
            });

            //when close the stream event from lpr camera
            memStream.on('close', async function () {
                log.error('Closing stream with LPR camera :' + lprCamera.Name);
                //cameras[response.config.headers.Ip]['errorCode'] = "1";
                lprCamera['InStreaming'] = false;
                //memStream.end();
                memStream.destroy();
                response.cancel();

                //try connect again to the camera
                //await listeningToEvent(lprCamera);

                //afetr the socket failes
                setTimeout(async () => {
                    //check if the lpr camera is alive but the socket is null 
                    if (lprCamera["IsAlive"] && !lprCamera["InStreaming"]) {
                        //try connect again to the camera
                        await listeningToEvent(lprCamera);
                    }
                }, 30000);
            });

            //when get error from sream lpr camera
            memStream.on('error', async function (error) {
                //cameras[response.config.headers.Ip]['errorCode'] = "1";
                log.error('Error in stream of LPR camera :' + lprCamera.Name + ", the streaming stop with error," + error.message);
                lprCamera['InStreaming'] = false;
                memStream.end();
                memStream.destroy();
                response.cancel();

                //try connect again to the camera
                //await listeningToEvent(lprCamera);
            });
        } else {

        }
    } catch (ex) {
        lprCamera['InStreaming'] = false;
        log.error("Error when listinning to stream: " + lprCamera.Name + ", Error:" + ex.message)
    }
}

//check if in the stream there is a new event
/* function checkStreamData(lprCamera) {
    let start = lprCamera['StremingData'].search("<EventNotificationAlert");
    let end = lprCamera['StremingData'].search("</EventNotificationAlert>");

    if (start != -1 && end != -1) {
        let xmlData = lprCamera['StremingData'].slice(start, end + 25);
        //log.specialMsg('XmlData length: ' + xmlData.length);
        //log.specialMsg('StreamingData length: ' + lprCamera['StremingData'].length);

        let jsonResponse = globalFunction.convertXmlToJson(xmlData);

        let eventName = jsonResponse["EventNotificationAlert"]["eventType"]["_text"];

        //if we found a complate message in the stream 
        //and we analyzed her information, so clean her
        cleanStreamData(lprCamera, end);

        //update that we get new message
        eventFromLpr.newEvent(lprCamera, eventName);
    }
} */

function checkStreamData(lprCamera) {
    let start = lprCamera['StremingData'].search("Content-Length:");
    let end = lprCamera['StremingData'].search("\r\n\r\n");

    if (start != -1 && end != -1) {
        let contentLength = +lprCamera['StremingData'].slice(start + 16, end);
        let xmlData = lprCamera['StremingData'].slice(0, contentLength + end + 4);

        //log.specialMsg('StreamingData length: ' + contentLength);

        if (xmlData.length == contentLength + end + 4) {
            //clean the event that we found in the stream
            cleanStreamData(lprCamera, xmlData.length);

            //check that this data is xml data
            let typeData = xmlData.search("application/xml");
            if (typeData != -1) {
                //clear the begining of thr message
                xmlData = xmlData.slice(end + 4, xmlData.length);

                //convert it to json 
                let jsonResponse;
                try {
                    jsonResponse = globalFunction.convertXmlToJson(xmlData);
                } catch (err) {
                    log.error("Error when try converting xml to json from Lpr camera:" + lprCamera.Name + ", Error: " + err.message);
                }

                //if success to converting data
                if (jsonResponse) {
                    let eventName = jsonResponse["EventNotificationAlert"]["eventType"]["_text"];

                    //update that we get new message
                    eventFromLpr.newEvent(lprCamera, eventName);
                }
            }
        }
    }
}

//clean the stream data
function cleanStreamData(lprCamera, end) {
    lprCamera['StremingData'] = lprCamera['StremingData'].substring(end, lprCamera['StremingData'].lenght);
}



module.exports.listeningToEvent = listeningToEvent;