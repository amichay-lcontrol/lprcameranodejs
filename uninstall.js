var Service = require('node-windows').Service; 
const path = require('path');
// Create a new service object

let scriptPath = path.resolve('index.js');
var svc = new Service({
  name: '!Shob_lprCamera-service',
    description: 'lprCamera-service',
    script: scriptPath
}); 
// Listen for the 'uninstall' event so we know when it is done.
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
}); 
// Uninstall the service.
svc.uninstall();
