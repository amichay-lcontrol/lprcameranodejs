// logger - for loggin our errors
const winston = require('winston');
const log = require('../global/console');

module.exports = function () {


    // handle uncaught exception
    // if some error accured during the startup this function will handle that error and log it 
    // the proccess will not terminate
    // works only with synchronized code
    winston.handleExceptions(

        // show error on the console
        new winston.transports.Console({
            colorize: true,
            prettyPrint: true
        }),
        // write error to the file
        new winston.transports.File({
            filename: 'uncaughtExceptions.log'
        }));

    // handle unhandled rejection
    // works with asynchronized code 
    // for example call to a database or call to a remote http service
    process.on('unhandledRejection', (ex) => {
        if (ex)
            log.error(`WE GOT ERROR REJECTION ${ex.message}`);
        else
            log.error(`WE GOT ERROR REJECTION as a ${ex}, with no error message`);
        winston.error(ex.message, ex);
        //throw ex;
    });

    // loggin to file
    winston.add(winston.transports.File, {
        filename: 'logfile.log',
        level: 'info'
    });

}