const io = require('socket.io-client');
const log = require("../global/console");
const settings = require('../settings.json');
const programControl = require("../program-control");
const lprParkingManagement = require("../protocols/lpr/general/parkingManagement");
const visitorsManagement = require("../protocols/lpr/general/visitorsManagment");

/* Hikvision  */
const lprHikvisionOpenGate = require("../protocols/lpr/hikvision/actions/openAndCloseGate");
const lprHikvisionDownloadWhiteList = require("../protocols/lpr/hikvision/actions/downloadWhiteList");
const lprHikvisionDownloadBlackList = require("../protocols/lpr/hikvision/actions/downloadBlackList");
const lprHikvisionRemoveWhiteList = require("../protocols/lpr/hikvision/actions/removeFromList");
const lprHikvisionKeepAliving = require("../protocols/lpr/hikvision/alive/keepAlivingWithLpr");

/* Provision */
const lprProvisionOpenGate = require("../protocols/lpr/provision/actions/openAndCloseGate");
const lprProvisionDownloadWhiteList = require("../protocols/lpr/provision/actions/downloadWhiteList");
const lprProvisionDownloadBlackList = require("../protocols/lpr/provision/actions/downloadBlackList");
const lprProvisionRemovePlate = require("../protocols/lpr/provision/actions/removePlate");
const lprProvisionKeepAliving = require("../protocols/lpr/provision/alive/keepAlivingWithLpr");
const stremingWithProvisionLpr = require("../protocols/lpr/provision/events/stremingWithLpr");


var socket;

function startSocket() {

    socket = io(settings.mainService, {
        query: {
            serviceName: 'camera-service',
            serviceSerialNumber: settings.serialNumber,
        }
    });

    socket.on('connect', () => {
        log.msg("socket connected");
    });

    socket.on('message', async (msg, callback) => {
        try {
            if (!msg.action)
                return;
            if (callback) {
                res = await checkMessageType(msg);
                callback(null, res);
            } else
                checkMessageType(msg.data);
        } catch (ex) {
            log.error(ex.message);

            if (callback)
                callback(ex, null);
        }
    });

    socket.on('disconnect', () => {
        log.alert(`socket disconnected`);
    });
}
startSocket();

function checkMessageType(data) {
    return new Promise(async (resolve, reject) => {
        try {
            switch (data.action) {
                case 'reload_service': {
                    try {
                        log.msg("WS - Get action to refresh service, from: " + data.from);

                        //close the socket with the lpr cameras
                        for (let i = 0; i < programControl.lprCameras.length; i++) {
                            //HIKVISION
                            if (programControl.lprCameras[i].TypeId == 1) {
                                lprHikvisionKeepAliving.clearAliveLprTimeout(programControl.lprCameras[i]);
                                if (programControl.lprCameras[i]["memStream"])
                                    programControl.lprCameras[i]["memStream"]["_events"].end();
                            }

                            //PROVISION
                            else if (programControl.lprCameras[i].TypeId == 2) {
                                lprProvisionKeepAliving.clearAliveLprTimeout(programControl.lprCameras[i]);
                                if (programControl.lprCameras[i]["socket"]) {
                                    stremingWithProvisionLpr.setUnSubScribeToSocket(programControl.lprCameras[i]);
                                }
                            }
                            programControl.lprCameras.splice(i, 1);
                            i--;
                        }

                        await programControl.resetInitRun();
                        await programControl.startProgram();
                        resolve(true);
                    } catch (ex) {
                        log.error(`reload_service error: ${ex.message}`);
                    }
                    break;
                }
                case 'alive_lpr_camera': {
                    log.msg("WS - Get action to check alive of lpr cameras, from: " + data.from);

                    for (let camera of data.cameras)
                        for (let lprCameraData of programControl.lprCameras)
                            if (camera.CameraId == lprCameraData.CameraId) {
                                camera["IsAlive"] = lprCameraData["IsAlive"];
                                camera["IsLpr"] = lprCameraData["IsLpr"];
                                break;
                            }

                    resolve(data.cameras);
                    break;
                }
                case 'open_gate': {
                    try {
                        log.msg("WS - Get action to open gate from: " + data.from);
                        let status;
                        //HIKVISION
                        if (data.cameras.TypeId == 1) {
                            status = await lprHikvisionOpenGate.openGate(data.cameras);
                        }

                        //PROVISION                        
                        else if (data.cameras.TypeId == 2) {

                            status = await lprProvisionOpenGate.openGateByTimerConfiguration(data.cameras);
                            if (!status)
                                throw (false);

                        }
                        resolve(status);
                    } catch (ex) {
                        log.error("Error get open gate response status:" + ex.message);
                        reject(false);
                    }
                    break;
                }
                case "download_white_list": {
                    try {
                        log.msg("WS - Get action to download white list from: " + data.from);
                        let statusArray = [];

                        //update data of  => users,visitors,cameras,company,  from DB
                        await programControl.getAndUpdateDataFromDB();

                        for (let lprCameraId of data.lprCameras) {
                            for (let lprCamera of programControl.lprCameras) {
                                if (lprCamera.CameraId == Number(lprCameraId)) {

                                    //HIKVISION
                                    if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                                        let status = await lprHikvisionDownloadWhiteList.downloadWhiteList(lprCamera, data.carList);

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    }

                                    //PROVISION
                                    if (lprCamera.TypeId == 2 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                                        let status = await lprProvisionDownloadWhiteList.addPlateToWhiteList(lprCamera, data.carList);

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    }
                                }
                            }
                        }

                        //check if this is cars that need to remove from the list
                        //lprParkingManagement.checkCarNumberNeedToRemove();

                        resolve(statusArray);
                    } catch (ex) {
                        reject(false);
                    }
                    break;
                }
                case "download_black_list": {
                    log.msg("WS - Get action to download black list, from: " + data.from);
                    let statusArray = [];

                    try {
                        for (let lprCameraId of data.lprCameras) {
                            for (let lprCamera of programControl.lprCameras) {
                                if (lprCamera.CameraId == Number(lprCameraId)) {

                                    //HIKVISION
                                    if (lprCamera.TypeId == 1 && lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                                        let status = await lprHikvisionDownloadBlackList.downloadBlackList(lprCamera, data.carList);

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    }

                                    //PROVISION
                                    if (lprCamera.TypeId == 2) {
                                        let status = await lprProvisionDownloadBlackList.addPlateToBlackList(lprCamera, data.carList);
                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    }
                                }
                            }
                        }

                        resolve(statusArray);
                    } catch (ex) {
                        reject(false);
                    }
                    break;
                }
                case "remove_from_list": {
                    log.msg("WS - Get action to remove from list, from: " + data.from);
                    let statusArray = [];

                    for (let lprCameraId of data.lprCameras) {
                        for (let lprCamera of programControl.lprCameras) {
                            if (lprCamera.CameraId == Number(lprCameraId)) {

                                //HIKVISION
                                if (lprCamera.TypeId == 1) {
                                    if (lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                                        let status = await lprHikvisionRemoveWhiteList.removeFromList(lprCamera, data.carList);

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    } else {
                                        let carStatus = [];
                                        for (let car of data.carList)
                                            carStatus.push({
                                                car: car,
                                                status: false
                                            })

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name,
                                                Message: "Lpr camera not connected"
                                            },
                                            carStatus: carStatus
                                        })
                                    }
                                }

                                //PROVISION
                                else if (lprCamera.TypeId == 2) {
                                    if (lprCamera["IsAlive"] && lprCamera["IsLpr"]) {
                                        let status = await lprProvisionRemovePlate.removeFromPlatesFromList(lprCamera, data.carList);

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name
                                            },
                                            carStatus: status
                                        })
                                    } else {
                                        let carStatus = [];
                                        for (let car of data.carList)
                                            carStatus.push({
                                                car: car,
                                                status: false
                                            })

                                        statusArray.push({
                                            lprCamera: {
                                                Name: lprCamera.Name,
                                                Message: "Lpr camera not connected"
                                            },
                                            carStatus: carStatus
                                        })
                                    }
                                }
                            }
                        }
                    }
                    resolve(statusArray);
                    break;
                }
                case 'get_status': {
                    log.msg('WS - Get action to get parking management status');

                    let response = lprParkingManagement.getParkingManagement(data);
                    resolve(response);
                    break;
                }
                case 'remove_occupied_parking': {
                    log.msg('WS - Get action to remove occupied parking');

                    let response = lprParkingManagement.removeOccupiedParking(data);
                    resolve(response);
                    break;
                }
                case 'update_init_data': {
                    log.msg('WS - Get action to update init data');

                    let response = programControl.getAndUpdateDataFromDB();
                    resolve(true);
                    break;
                }
                case 'get_allowd_status_of_car_number_in_lpr_camera': {
                    log.msg('WS - Get action to get staus of car number in the lpr camera');
                    let statusArray = [];

                    try {
                        for (let lprCameraId of data.lprCameras) {
                            for (let lprCamera of programControl.lprCameras) {
                                if (lprCamera.CameraId == Number(lprCameraId)) {

                                    if (lprCamera["IsAlive"]) {

                                        statusArray.push({
                                            lprCamera: {
                                                Id: lprCamera.CameraId,
                                                status: true,
                                                cars: []
                                            }
                                        })
                                        for (let carNumber of data.carNumbers) {
                                            let car = await lprParkingManagement.checkCarNumberStatusInLprCamera(lprCamera, carNumber);

                                            statusArray[statusArray.length - 1].lprCamera.cars.push(car);
                                        }
                                    } else {
                                        statusArray.push({
                                            lprCamera: {
                                                Id: lprCamera.CameraId,
                                                status: false,
                                                cars: []
                                            }
                                        })
                                    }
                                }
                            }
                        }

                    } catch (ex) {
                        log.error("Error in connect to lpr camera, Error: " + ex.message);
                    }


                    resolve(statusArray);
                    break;
                }
                case 'update_visitor': {
                    log.msg('WS - Get action to visitor data in service');

                    try {
                        let status = await visitorsManagement.updatVisitor(data.visitorData);

                        resolve(status);
                    } catch (ex) {
                        log.error("Error in update visitor, Error: " + ex.message);
                        reject(ex.message)
                    }

                    break;
                }
                case 'update_lpr_camera': {
                    log.msg('WS - Get action to update lpr camera data');

                    programControl.resetInitRun();
                    programControl.startProgram();
                    resolve(true);
                    break;
                }
                default: {
                    resolve('no such case');
                    break;
                }
            }
        } catch (ex) {
            reject(new Error(ex.message));
        }
    });
}