// Dear programmer: 
// When I wrote this code, only god and I knew how it worked. 
// Now, only god knows it.

// Therefore, if you are trying to optimize 
// This routine and it fails (most surely),
// Please increase this counter as a + 1
// Warning for the next person: 
// Total_hours_wasted_here = ?????
// Con amor Amichay



const http = require('./http/http');
const log = require("./global/console");
const fs = require('fs');
const lprHikvision = require("./protocols/lpr/hikvision/alive/connectionWithLpr");
const lprProvision = require('./protocols/lpr/provision/alive/connectionWithLpr');
const parkingManagement = require("./protocols/lpr/general/parkingManagement");
const visitorsManagement = require("./protocols/lpr/general/visitorsManagment");

var lprCameras = [];
var initRun = true;
var indexOfConnectionTimes = 0;
var connectiionToInternet;

async function startProgram() {

    //in the firs start
    if (initRun) {
        log.specialMsg('starting program...');
        initRun = false;

        //get all lpr camera,user,company data from DB
        let response = await getAndUpdateDataFromDB();

        //if connect to internet
        if (response) {
            connectiionToInternet = true;

            //connect to the lpr camera, check alive, if this is LPR camera, and start listener to event from lpr camere
            /*   try {
                  //let x = readdirSync("C:/Amichay/Document");
                  //console.log(x);
                  //fs.readFileSync(new URL('file://c:/path/to/file'));
                  let cameras = fs.readFileSync(new URL('file:///c:/Amichay/Document/cameras.txt'));
                  console.log(cameras.toString());
              } catch (ex) {
                  log.error("Error read file , " + ex.message)
              } */



            //connect to the lpr camera, check alive, if this is LPR camera, and start listener to event from lpr camere
            let cameras = fs.readFileSync("cameras.txt");
            cameras = JSON.parse(cameras);

            for (let camera of cameras) {
                lprCameras.push(camera);

                //HIKVISION
                if (camera.TypeId == 1)
                    lprHikvision.getCamerasAlive(camera);

                //PROVISION
                if (camera.TypeId == 2) {
                    lprProvision.getCamerasAlive(camera);
                }
            }
        }

        //when no connected to innternet
        else {
            connectiionToInternet = false;
            log.error("Error when try get the data from main srvice");

            //check if exist the files , use the data from files
            let filesExist = checkIfFilesExist();
            if (filesExist) {
                log.info("the internet not connected, so we work with the data that on the server")
                //connect to the lpr camera, check alive, if this is LPR camera, and start listener withe the lpr camere
                let cameras = fs.readFileSync("cameras.txt");
                cameras = JSON.parse(cameras);

                for (let camera of cameras) {
                    lprCameras.push(camera);

                    //HIKVISION
                    if (camera.TypeId == 1)
                        lprHikvision.getCamerasAlive(camera);

                    //PROVISION
                    if (camera.TypeId == 2) {
                        lprProvision.getCamerasAlive(camera);
                    }
                }
            } else {
                log.error("Cannot start lpr service, this not connect to internet , and for first time must to be some files.");
                log.info("Connect to internet at list for first connection just for get the data that is need");
            }
        }


        //wait 6 secounds until will update the camera (isAlive?, isLpr?)
        setTimeout(async function () {
            if (lprCameras.length > 0) {
                //update the white list in the lpr camera
                //await parkingManagement.updateAllowedCarsInParkingManagment();

                //start the interval that will update the visitors
                await visitorsManagement.checkForNeedsUpdateInVisitorData();
            }
        }, 6000)
    }
    //if is restart
    else {
        indexOfConnectionTimes++;
        log.specialMsg('Starting connect again, times: ' + indexOfConnectionTimes);
    }
}
startProgram();

//this function get lpr camera useres and company from data base and upadte it in the files
async function getAndUpdateDataFromDB() {
    let companyParking = {};

    try {
        //get data of lpr cameras,companies,users,visitors  from DB
        let data = await http.getInitData();

        if (data) {
            //save the lpr cameras
            fs.writeFileSync("cameras.txt", JSON.stringify(data.cameras));


            //save the users
            fs.writeFileSync("users.txt", JSON.stringify(data.users));

            //save the existing car by company
            //get the exist car list from the file
            let existingCarData;
            try {
                existingCarData = fs.readFileSync("company_parking_count.txt");
                existingCarData = JSON.parse(existingCarData);
            } catch (ex) {
                log.error("error when try read file `company_parking_count.txt`, Error:" + ex.message)
            }

            for (let company of data.companyParkingCount)
                companyParking[company.CompanyId] = {
                    existingCars: !existingCarData || !existingCarData[company.CompanyId] ? [] : existingCarData[company.CompanyId].existingCars,
                    ParkingCount: existingCarData && existingCarData[company.CompanyId] ? (company.ParkingCount - existingCarData[company.CompanyId].existingCars.length) : company.ParkingCount
                }
            fs.writeFileSync("company_parking_count.txt", JSON.stringify(companyParking));


            //save the visitors
            let visitorsData = {
                needsToEnter: [],
                needsToExit: []
            }
            let date = new Date();
            for (let visitor of data.visitors) {
                if (new Date(visitor.FromDate) >= date)
                    visitorsData.needsToEnter.push(visitor);
                else
                    visitorsData.needsToExit.push(visitor);
            }
            fs.writeFileSync("visitors.txt", JSON.stringify(visitorsData));

            return true;
        } else {
            log.error("Not response with data when try read and update files data");
            return false;
        }
    } catch (ex) {
        log.error("Error when try read and update files data, Error: " + ex.message);
        return false;
    }

    //affter changes in usersor parkingManegement data we also update the main service
    /*    try {
           let parkingData = {};

           for (let key of Object.keys(companyParking))
               parkingData[key] = {
                   cars: companyParking[key].existingCars,
                   occupiedParkingSpace: companyParking[key].existingCars.length
               }

           //update main
           let response = await http.updateOccupiedParkingSpace(parkingData);
       } catch (ex) {
           log.error("Error when try send update to main, Error: " + ex.message);
       } */
}

function resetInitRun() {
    initRun = true;
}

function checkIfFilesExist() {
    let cameraFile;
    let usersFile;
    let parkingManagementFile;
    let visitors;

    //check if exist file : cameras.txt
    try {
        fs.readFileSync("cameras.txt");
        cameraFile = true;
    } catch (ex) {
        cameraFile = false;
        log.error("not found cameras.txt file, " + ex.message);
    }

    //check if exist file : users.txt
    try {
        fs.readFileSync("users.txt");
        usersFile = true;
    } catch (ex) {
        usersFile = false;
        log.error("not found users.txt file, " + ex.message);
    }

    //check if exist file : company_parking_count.txt
    try {
        fs.readFileSync("company_parking_count.txt");
        parkingManagementFile = true;
    } catch (ex) {
        parkingManagementFile = false;
        log.error("not found company_parking_count.txt file, " + ex.message);
    }

    //check if exist file : visitors.txt
    try {
        fs.readFileSync("visitors.txt");
        visitors = true;
    } catch (ex) {
        visitors = false;
        log.error("not found visitors.txt file, " + ex.message);
    }




    if (cameraFile && usersFile && parkingManagementFile && visitors)
        return true;
    else
        return false;
}

module.exports.getAndUpdateDataFromDB = getAndUpdateDataFromDB;
module.exports.resetInitRun = resetInitRun;
module.exports.startProgram = startProgram;
module.exports.lprCameras = lprCameras;