const convert = require('xml-js');


const convert_option = {
    compact: true,
    spaces: 4
}


// this function get xml data and return json
function convertXmlToJson(xml) {
    let jsonResponse = convert.xml2json(xml, convert_option);
    jsonResponse = JSON.parse(jsonResponse);


    return jsonResponse;
}

//this function get json and return xml
function convertJsonToXml(jsonResponse) {
    let xml = convert.js2xml(jsonResponse, convert_option);

    return xml;
}

module.exports.convertXmlToJson = convertXmlToJson;
module.exports.convertJsonToXml = convertJsonToXml;