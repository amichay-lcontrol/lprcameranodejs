var Service = require('node-windows').Service;
const path = require('path');

let scriptPath = path.resolve('index.js');

var svc = new Service({
    name: '!Shob_lpr-camera-service',
    description: 'lpr-camera-service',
    script: scriptPath
});
svc.on('install', function () {
    svc.start();
});
svc.install();