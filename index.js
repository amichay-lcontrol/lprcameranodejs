// logging errors to files
require('./startup/logging')();

// start the handle the socket
require('./startup/ws');

// start the program control
require('./program-control');