const log = require("../global/console");
const axios = require('axios');
const settings = require('../settings.json');



// bnring from the main the data all the related cameras
function getInitData() {
    return new Promise(async (resolve, reject) => {
        try {
            log.msg('Getting init data...');
            let url = settings.mainService + "/cameras/get-lpr-init-data/" + settings.serialNumber;
            let response = await axios.get(url);

            log.info('Success on getting init data.');
            resolve(response.data);
        } catch (ex) {
            log.error('Failed to get init data: ' + ex.message);
            reject(ex);
        }
    });
}

//update the main if change happend withe the alive of LPR camera
async function lprCameraAlive(aliveUpdate) {
    //beafore update mein service check if ther is connection to the internet
    let checkInternetConnection = await checkConnectionToInternet();

    //when connect to internet
    if (checkInternetConnection) {
        return new Promise(async (resolve, reject) => {
            try {
                log.msg('sending updateing in alive to the main servie');
                let url = settings.mainService + "/cameras/update-alive-lpr-camera/";
                let response = await axios.post(url, aliveUpdate);

                if (response.status) {
                    log.info('Success on updateing alive in main service');
                    resolve(response.data);
                }
            } catch (ex) {
                log.error('Failed to updateing alive in main service: ' + ex.message);
                reject(ex);
            }
        });
    }

    //when not have internet save the changes in file
    else {
        log.msg("No internet connection, so we save update main on change in alive camera in the file");

    }
}

//update the main that there is a new LPR camera event
async function updateNewLprCameraEvent(newEvent) {
    //beafore update mein service check if ther is connection to the internet
    let checkInternetConnection = await checkConnectionToInternet();

    //when connect to internet
    if (checkInternetConnection) {
        return new Promise(async (resolve, reject) => {
            try {
                log.msg('sending new event to the main servie');
                let url = settings.mainService + "/cameras/new-lpr-camera-event";
                let response = await axios.post(url, newEvent);

                if (response.status) {
                    log.info('Success on updateing new event in main service');
                    resolve(response.data);
                }
            } catch (ex) {
                log.error('Failed to updateing new event in main service: ' + ex.message);
                reject(ex);
            }
        });
    }

    //when not have internet save the changes in file
    else {
        log.msg("No internet connection, so we save New Event data in the file");

    }
}


async function updateOccupiedParkingSpace(parkingData) {
    //beafore update mein service check if ther is connection to the internet
    let checkInternetConnection = await checkConnectionToInternet();

    //when connect to internet
    if (checkInternetConnection) {
        return new Promise(async (resolve, reject) => {
            try {
                log.msg('sending parking managment to the main servie');
                let url = settings.mainService + "/cameras/parking-management/update/" + settings.serialNumber;
                let response = await axios.post(url, parkingData);

                if (response.status) {
                    log.info('Success on updateing parking managment in main service');
                    resolve(response.data);
                }
            } catch (ex) {
                log.error('Failed to updateing parking managment in main service: ' + ex.message);
                reject(ex);
            }
        });
    }

    //when not have internet save the changes in file
    else {
        log.msg("No internet connection, so we save Occupied Parking Space data in the file");

    }
}


async function checkConnectionToInternet() {
    return new Promise(async (resolve, reject) => {
        try {
            require('dns').resolve('www.google.com', function (err) {
                if (err) {
                    log.info("No connection to internet");
                    resolve(false);
                } else {
                    log.info("Connected to internet");
                    resolve(true);
                }
            });
        } catch (err) {
            log.error("Error when try check connection with the internet")
        }
    })
}



module.exports.getInitData = getInitData;
module.exports.updateNewLprCameraEvent = updateNewLprCameraEvent;
module.exports.updateOccupiedParkingSpace = updateOccupiedParkingSpace;
module.exports.lprCameraAlive = lprCameraAlive;